#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE ROLE andata WITH LOGIN PASSWORD 'andata';
    ALTER ROLE andata CREATEDB;
    CREATE DATABASE andata OWNER andata ENCODING 'UTF8';
EOSQL
