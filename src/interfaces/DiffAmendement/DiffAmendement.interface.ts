import { Amendement } from 'src/entity/Amendements';

/**
 * Interface for describing diff files from diff process
 * used in tricoteuses-assemblee
 */
export interface DiffAmendement{
    updated?: {
        amendement?: Amendement;
    };
    deleted?: {
        amendement?: Amendement;
    }
    added?: {
        amendement?: Amendement;
    }
}
