export enum SortEnSeance {
    ADOPTE = 'Adopté',
    NON_SOUTENU = 'Non soutenu',
    REJETE = 'Rejeté',
    RETIRE = 'Retiré',
    TOMBE = 'Tombé',
}
