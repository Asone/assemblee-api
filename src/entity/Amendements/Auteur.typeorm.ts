import { TypeAuteur } from '.';

import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { Field, ObjectType, registerEnumType } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

registerEnumType(TypeAuteur, {
    name: "TypeAuteur", // this one is mandatory
    description: "Type d\'auteur", // this one is optional
})

@EsEntity()
@ObjectType({
    description: 'Auteur de l\'amendement'
})
@Entity({
    name: 'Auteur',
})
export class Auteur {


    constructor(auteur: Auteur = undefined){
        if(auteur){
            this.id = auteur.id;
            this.typeAuteur = auteur.typeAuteur;
            this.acteurRef = auteur.acteurRef;
            this.groupePolitiqueRef = auteur.groupePolitiqueRef;
            this.organeRef = auteur.organeRef;
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @Field(type => TypeAuteur,{
        description: 'Type d\'auteur',
        nullable: true
    })
    @Column('enum',
    {
        name: 'type_auteur',
        enum: TypeAuteur,
        default: TypeAuteur.DEPUTE,
        nullable: true
    })
    public typeAuteur?: TypeAuteur;

    @EsField({
        type: 'text',
    })
    @Field({
        description: 'réference d\‘acteur pour l\'auteur de l\‘amendement',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public acteurRef?: string;

    @EsField({
        type: 'text',
    })
    @Field({
        description: 'organe de Référence à travers lequel l\'auteur intervient',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public organeRef?: string;

    @EsField({
        type: 'text',
    })
    @Field({
        description: 'rattachement de groupe politique de l\'auteur',
        nullable: true
    })
    @Column({ type: 'text', nullable: true })
    public groupePolitiqueRef?: string;
}
