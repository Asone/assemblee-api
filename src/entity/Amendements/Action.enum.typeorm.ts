export enum Action {
    CREATION = 'creation',
    MODIFICATION = 'modification',
    SUGGESTION = 'suppression',
}
