export enum Etat {
    A_DISCUTER = 'A discuter',
    DISCUTE = 'Discuté',
    EN_RECEVABILITE = 'En recevabilité',
    EN_TRAITEMENT = 'En traitement',
    IRRECEVABLE = 'Irrecevable',
    RETIRE = 'Retiré',
}
