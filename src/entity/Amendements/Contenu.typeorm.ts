export interface Contenu {
    documentURI: string;
}

import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Contenu de la représentation',
})
@Entity({
    name: 'Contenu',
})
export class Contenu {

    constructor(contenu: Contenu = undefined){
        if(contenu){
            this.id = contenu.id;
            this.documentURI = contenu.documentURI;
        }
    }
    
    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'URL du document',
        nullable: true
    })
    @Column({type: 'text', nullable: true })
    public documentURI: string;

}
