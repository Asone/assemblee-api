import { Entity, PrimaryGeneratedColumn, JoinColumn, OneToOne } from 'typeorm';

import { MissionVisee, Division } from '.';
import { Alinea } from './Alinea.typeorm'; 
import { Field, ObjectType } from 'type-graphql';

@ObjectType({
    description: 'Pointeur de fragment de texte'
})
@Entity({
    name: 'PointeurFragmentTexte',
})
export class PointeurFragmentTexte {

    constructor(pointeurFragmentTexte: PointeurFragmentTexte = undefined){

        if(pointeurFragmentTexte){
            this.id = pointeurFragmentTexte.id
            this.missionVisee = new MissionVisee(pointeurFragmentTexte.missionVisee);
            this.division = new Division(pointeurFragmentTexte.division);
            this.alinea = new Alinea(pointeurFragmentTexte.alinea);
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @Field(type => MissionVisee,{
        description: 'Mission visée',
        nullable: true
    })
    @OneToOne(type => MissionVisee,{
        nullable: true,
        eager: true, cascade: ['update','remove']
    })
    @JoinColumn()
    public  missionVisee?: MissionVisee;

    @Field(type => Division,{
        description: 'Division de texte',
        nullable: true
    })
    @OneToOne(type => Division,{
        nullable: true,
        eager: true, cascade: ['update','remove']
    })
    @JoinColumn()
    public division?: Division;

    @Field(type => Alinea,{
        description: 'Alinea',
        nullable: true
    })
    @OneToOne(type => Alinea,{
        nullable: true,
        eager: true, cascade: ['update','remove']
    })
    @JoinColumn()
    public alinea: Alinea;
}
