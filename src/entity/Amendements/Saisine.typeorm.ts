import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { ObjectType, Field } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Saisine de l\'amendement'
})
@Entity({
    name: 'Saisine',
})
export class Saisine {

    constructor(saisine: Saisine = undefined){
        if(saisine){
            this.id = saisine.id;
            this.refTexteLegislatif = saisine.refTexteLegislatif;
            this.numeroPartiePLF = saisine.numeroPartiePLF;
            this.mentionSecondeDeliberation = saisine.mentionSecondeDeliberation;
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'text'
    })
    @Field({
        nullable: true,
        description: 'Référence du texte législatif auquel est associé l\'amendement'
    })
    @Column({type : 'text', nullable: true})
    public refTexteLegislatif?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        nullable: true,
        description: 'Référence de la partie PLF représentée'
    })
    @Column({type : 'text', nullable: true})
    public  numeroPartiePLF?:  string;

    @EsField({
        type: 'text'
    })
    @Field({
        nullable: true,
        description: 'Organe ayant procédé à l\'exament de l\'amendement'
    })
    @Column({type : 'text', nullable: true})
    public organeExamen?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        nullable: true,
        description: 'mention d\'une seconde délibération'
    })
    @Column({type : 'text', nullable: true})
    public mentionSecondeDeliberation?: string;
}
