import { Column, Entity, PrimaryGeneratedColumn, JoinColumn, OneToOne } from 'typeorm';
import { Contenu } from './Contenu.typeorm';
import { Nom } from './Nom.enum.typeorm';
import { StatutRepresentation } from './StatutRepresentation.typeorm';
import { TypeMimeType } from './TypeMimeType.enum.typeorm';
import { ObjectType, Field, registerEnumType } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

registerEnumType(Nom, {
    name: "Nom", // this one is mandatory
    description: "Nom du type de représentation", // this one is optional
})

@EsEntity()
@ObjectType({
    description: 'Representation de l\'amendement',
})
@Entity({
    name: 'Representation',
})
export class Representation {

    constructor(representation: Representation = undefined){
        if(representation){
            this.id = representation.id;
            this.nom = representation.nom;
            this.statutRepresentation = new StatutRepresentation(representation.statutRepresentation);
            this.repSource = representation.repSource;
            this.offset = representation.offset;
            this.contenu = new Contenu(representation.contenu);
            this.dateDispoRepresentation = representation.dateDispoRepresentation;
        }
    }
    @PrimaryGeneratedColumn()
    public id: string;

    @Field(type => Nom,{
        nullable: true,
        description: 'Type de représentation'
    })
    @Column('enum',
    {
        name: 'nom',
        enum: Nom,
        default: Nom.PDF,
        nullable: true
    })
    public  nom: Nom;

    @Column('enum',
    {
        enum: TypeMimeType,
        default: TypeMimeType.APPLICATION,
    })
    public typeMime?: TypeMimeType;

    @Field(type => StatutRepresentation,{
        description: 'Statut de représentation',
        nullable: true
    })
    @OneToOne(type => StatutRepresentation, { cascade: ['update','remove']})
    @JoinColumn()
    public statutRepresentation?: StatutRepresentation;

    @EsField({
        type: 'keyword'
    })
    @Field({
        description: 'repSource',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public repSource?: string;

    @EsField({
        type: 'keyword'
    })
    @Field({
        description: 'Position d\'offset',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public offset?: string;

    @Field(type => Contenu,{
        description: 'Contenu',
        nullable: true
    })
    @OneToOne(type => Contenu,{
        nullable: true, cascade: ['update','remove']
    })
    @JoinColumn()
    public contenu?: Contenu;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Date de disponibilité de la représentation',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public dateDispoRepresentation?: string;
}
