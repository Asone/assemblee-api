import { AvantAApresEnum } from '.';

import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { ObjectType, registerEnumType , Field } from 'type-graphql';


registerEnumType(AvantAApresEnum, {
    name: "AvantAApres", // this one is mandatory
    description: "Positionnement dans le texte", // this one is optional
})

@ObjectType({
    description: 'Alinea de l\'amendement',
})
@Entity({
    name: 'Alinea',
})
export class Alinea {

    constructor(alinea: Alinea = undefined){
        if(alinea){
            // this.id = alinea.id;
            this.id = alinea.id;
            this.avant_A_Apres = alinea.avant_A_Apres;
            this.numero = alinea.numero;
            this.alineaDesignation = alinea.alineaDesignation;
        }
    }
    
    @PrimaryGeneratedColumn()
    public id: string;

    @Field(type => AvantAApresEnum,{
        description: 'Etape du texte',
        nullable: true
    })
    @Column('enum', {
        name: 'avant_a_apres',
        enum: AvantAApresEnum,
        default: AvantAApresEnum.A,
        nullable: true
    })
    public  avant_A_Apres?: AvantAApresEnum;

    @Field({
        nullable: true,
        description: 'numéro',
    })
    @Column({type: 'text', nullable: true})
    public numero?: string;

    @Field({
        nullable: true,
        description: 'Désignation de l\'alinéa',
    })
    @Column({type: 'text', nullable: true})
    public alineaDesignation?: string;
}
