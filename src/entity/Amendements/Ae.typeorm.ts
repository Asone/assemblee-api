import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { ObjectType, Field } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Ae',
})
@Entity({
    name: 'Ae',
})
export class Ae {

    constructor(ae: Ae = undefined){
        if(ae){
            // this.id = ae.id;
            this.montantPositif = ae.montantPositif;
            this.montantNegatif = ae.montantNegatif
        }
    }
    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Montant positif',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public montantPositif?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Montant positif',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public montantNegatif?: string;
}
