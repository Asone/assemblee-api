import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { ObjectType, Field } from 'type-graphql';

@ObjectType({
    description: 'Dispositif de crédit relatif à l\'amendement',
})
@Entity({
    name: 'StatutRepresentation',
})
export class StatutRepresentation {

    constructor(statutRepresentation: StatutRepresentation = undefined){
        if(statutRepresentation){
            this.id = statutRepresentation.id;
            this.verbatim = statutRepresentation.verbatim;
            this.canonique = statutRepresentation.canonique;
            this.officielle = statutRepresentation.officielle;
            this.transcription = statutRepresentation.transcription;
            this.enregistrement = statutRepresentation.enregistrement;
        }
    }
    
    @PrimaryGeneratedColumn()
    public id: string;

    @Field({
        description: 'Verbatim',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public verbatim: string;

    @Field({
        description: 'canonique',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public canonique: string;

    @Field({
        description: 'officielle',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public officielle: string;

    @Field({
        description:'Transcription',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public transcription: string;

    @Field({
        description:'Enregistrement',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public enregistrement: string;
}
