export enum EtapeTexte {
    DEUXIEME_LECTURE = 'deuxième lecture',
    LECTURE_DEFINITIVE = 'Lecture définitive',
    LECTURE_TEXTE_CMP = 'Lecture texte CMP',
    LECTURE_UNIQUE = 'Lecture unique',
    NOUVELLE_LECTURE = 'Nouvelle Lecture',
    PREMIERE_LECTURE_PREMIERE_ASSEMBLEE_SAISIE = '1ère lecture (1ère assemblée saisie)',
    PREMIERE_LECTURE_DEUXIEME_ASSEMBLEE_SAISIE = '1ère lecture (2ème assemblée saisie)',
}
