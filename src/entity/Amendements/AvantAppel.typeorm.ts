
import {
    Column,
    Entity,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from 'typeorm';
import { ObjectType, Field } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

// import { User } from './User';

@EsEntity()
@ObjectType({
    description: 'Avant appel',
})
@Entity({
    name: 'AvantAppel',
})
export class AvantAppel {

    constructor(avantAppel: AvantAppel = undefined){
        if(avantAppel){
            this.id = avantAppel.id;
            this.dispositif = avantAppel.dispositif;
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'dispositif',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public dispositif?: string;

}
