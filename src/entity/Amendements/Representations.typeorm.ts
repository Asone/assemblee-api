import { Representation } from '.';

import {
    Entity,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne
} from 'typeorm';
import { ObjectType, Field } from 'type-graphql';
import { EsEntity, EsField } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Représentations de l\'amendement',
})
@Entity({
    name: 'Representations',
})
export class Representations {

    constructor(representations: Representations = undefined){
        if(representations){
            this.id = representations.id;
            this.representation = new Representation(representations.representation);
        }
    }
    
    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'object',
        fieldClass: Representation
    })
    @Field(type => Representation,{
        description: 'Représentation',
        nullable: true
    })
    @OneToOne(type => Representation, { nullable: true, cascade: ['update','remove'] })
    @JoinColumn()
    public representation: Representation;
}
