import { DivisionType, AvantAApresEnum } from '.';

import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { registerEnumType, ObjectType, Field } from 'type-graphql';
import { EsEntity, EsField } from 'es-mapping-ts';

registerEnumType(AvantAApresEnum, {
    name: "AvantAApres", // this one is mandatory
    description: "Positionnement de l\'amendement par rapport à la division du texte", // this one is optional
})

registerEnumType(DivisionType, {
    name: "DivisionType", // this one is mandatory
    description: "Type de division", // this one is optional
})

@EsEntity()
@ObjectType({
    description: 'Division du texte'
})
@Entity({
    name: 'Division',
})
export class Division {

    constructor(division: Division = undefined){
        if(division){
            this.id = division.id;
            this.articleAdditionnel = division.articleAdditionnel;
            this.articleDesignationCourte = division.articleDesignationCourte;
            this.urlDivisionTexteVise = division.urlDivisionTexteVise;
            this.divisionType = division.divisionType;
            this.avant_A_Apres = division.avant_A_Apres;
            this.chapitreAdditionnel = division.chapitreAdditionnel;
            this.titre = division.titre;
            this.divisionRattachee = division.divisionRattachee;
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;
    
    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Titre',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public titre?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Désignation courte de l\'article',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public articleDesignationCourte?: string;

    @Field(type => DivisionType,{
        description: 'Type de division',
        nullable: true
    })
    @Column('enum',
    {
        name: 'type',
        enum: DivisionType,
        default: DivisionType.ARTICLE,
    })
    public divisionType: DivisionType;

    @Field(type => AvantAApresEnum,{
        description: 'Avant/A/Apres',
        nullable: true
    })
    @Column('enum',
    {
        name: 'avant_a_apres',
        enum: AvantAApresEnum,
        default: AvantAApresEnum.A,
    })
    public avant_A_Apres: AvantAApresEnum;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Division rattachée',
        nullable: true
    })
    @Column({ type : 'text', nullable: true})
    public divisionRattachee?: string;
    
    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Article additionnel',
        nullable: true
    })
    @Column({ type : 'text', nullable: true})
    public articleAdditionnel?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Chapitre additionnel',
        nullable: true
    })
    @Column({ type : 'text', nullable: true})
    public chapitreAdditionnel?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'URL de la division du texte visé',
        nullable: true
    })
    @Column({ type : 'text', nullable: true})
    public urlDivisionTexteVise?: string;
}
