import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    OneToMany
} from 'typeorm';

import { LigneCreditElement } from './LigneCreditElement.typeorm';
import { ObjectType, Field } from 'type-graphql';
import { EsEntity, EsField } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Lignes de crédit',
})
@Entity({
    name: 'LignesCredits',
})
export class LignesCredits {

    constructor(lignesCredits: LignesCredits = undefined){
        if(lignesCredits){
            this.id = lignesCredits.id;
            this.ligneCredit = lignesCredits.ligneCredit.map((ligneCreditElement: LigneCreditElement) => {
                return new LigneCreditElement(ligneCreditElement);
            });
        }
    }
    @PrimaryGeneratedColumn()
    public id: string;
    
    @EsField({
        type: 'nested',
        fieldClass: LigneCreditElement
    })
    @Field(type => [LigneCreditElement],{
        description: 'Les différentes lignes de crédit relatives à l\'amendement',
        nullable: true
    })
    @OneToMany(type => LigneCreditElement, ligneCreditElement => ligneCreditElement.ligneCredit, { nullable: true })
    public ligneCredit?: LigneCreditElement[];

}
