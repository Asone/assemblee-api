import {
    Entity,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn} from 'typeorm';

import { Total } from './Total.typeorm';
import { ListeProgrammes } from './ListeProgrammes.typeorm';
import { ObjectType, Field } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Dispositif de crédit relatif à l\'amendement',
})
@Entity({
    name: 'DispositifAmdtCredit',
    orderBy: {
        id: 'ASC',
    },
})
export class DispositifAmdtCredit {

    constructor(dispositifAmdtCredit: DispositifAmdtCredit = undefined){
        if(dispositifAmdtCredit){
            this.id = dispositifAmdtCredit.id;
            this.listeProgrammes = new ListeProgrammes(dispositifAmdtCredit.listeProgrammes);
            this.totalAE = new Total(dispositifAmdtCredit.totalAE);
            this.totalCP = new Total(dispositifAmdtCredit.totalCP);
        }
    }
    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'object',
        fieldClass: ListeProgrammes
    })
    @OneToOne(type => ListeProgrammes, { nullable: true, cascade: ['update','remove'] })
    @JoinColumn()
    public listeProgrammes?: ListeProgrammes;

    @EsField({
        type: 'object',
        fieldClass: Total
    })
    @Field(type => Total, {
        description: 'Total pour l\'AE',
        nullable: true
    })
    @OneToOne(type => Total, { nullable: true, cascade: ['update','remove']})
    @JoinColumn()
    public totalAE?: Total;

    @EsField({
        type: 'object',
        fieldClass: Total
    })
    @Field(type => Total,{
        description: 'Total pour le CP',
        nullable: true
    })
    @OneToOne(type => Total, { nullable: true, cascade: ['update','remove'] })
    @JoinColumn()
    public totalCP?: Total;

}
