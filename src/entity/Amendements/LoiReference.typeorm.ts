import {
    Column,
    Entity,
    PrimaryGeneratedColumn } from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Référence de loi',
})
@Entity({
    name: 'LoiReference',
})
export class LoiReference {

    constructor(loiReference: LoiReference = undefined){
        if(loiReference){
            this.id = loiReference.id;
            this.codeLoi = loiReference.codeLoi;
            this.divisionCodeLoi = loiReference.divisionCodeLoi;
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Code de la loi relative à l\'amendement',
        nullable: true
    })
    @Column({ type: 'text', nullable: true })
    public  codeLoi?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Division de référence du code de la loi relative à l\'amendement',
        nullable: true
    })
    @Column({ type: 'text', nullable: true })
    public  divisionCodeLoi?: string;

}
