import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import { EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Cosignataires de l\’amendement'
})
@Entity({
    name: 'Cosignataires',
})
export class Cosignataires {

    constructor(cosignataires: Cosignataires = undefined){
        if(cosignataires){
            this.id = cosignataires.id;
            this.acteurRef = cosignataires.acteurRef;
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @Field(type => [String],
    {
        description: 'références d\'acteur des cosignataires',
        nullable: true
    })
    @Column({type: 'simple-array', nullable: true  })
    public acteurRef?: string[];
}   
