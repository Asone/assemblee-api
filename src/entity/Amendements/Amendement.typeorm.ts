import { EtapeTexte, Etat, Signataires, Corps, PointeurFragmentTexte, Identifiant, Sort, LoiReference } from '.';
import { Representations } from './Representations.typeorm';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
    AfterInsert
} from 'typeorm';
import { ObjectType, Field, ID, registerEnumType } from 'type-graphql';
// import { Clotho } from 'clotho';
import { EsEntity, EsField } from 'es-mapping-ts';
import { Timestampable } from '../Abstracts/timestampable.typeorm';
import { ESImporterClient } from '../../lib/elastic-importer-client';
import { ApiResponse } from '@elastic/elasticsearch';
import { Logger } from '../../lib/logger';

registerEnumType(Etat, {
    name: "Etati", // this one is mandatory
    description: "Etat de procédure de l'amendement", // this one is optional
})

registerEnumType(EtapeTexte, {
    name: "EtapeTexte", // this one is mandatory
    description: "Etape du texte", // this one is optional
})

@EsEntity({
    index: 'amendement',
    type: 'Amendement'
})
@ObjectType({
    description: 'Amendement transaction',
})
@Entity({
    name: 'Amendement',
})
export class Amendement extends Timestampable {
    
    // For logging when providing some process in events
    // private logger: Logger = new Logger('Amendement Entity');
    
    /**
     * 
     * @param amendement The amendment to populate in our entity class
     */
    constructor(amendement: Amendement = undefined){
        super();
        
        /**
         * This will iterate each field of the provided amendment 
         * to populate the instance. 
         * 
         * This is useful for rendering Elastic search result as an Amendment
         * entity instance. 
         */
        if(amendement){

            Object.keys(amendement).forEach((key: string) => { // default patcher
                if(amendement[key]) { this[key] = amendement[key]; }
            });

            this.identifiant = new Identifiant(amendement.identifiant);
            this.signataires = new Signataires(amendement.signataires);
            this.pointeurFragmentTexte = new PointeurFragmentTexte(amendement.pointeurFragmentTexte);
            this.corps = new Corps(amendement.corps);
            this.representations = new Representations(amendement.representations);
            this.sort = new Sort(amendement.sort);
            this.loiReference = new LoiReference(amendement.loiReference);
            this.dateDepot = new Date(amendement.dateDepot);
            this.dateDistribution = new Date(amendement.dateDistribution);
        }
        // console.log(typeof this.dateDepot);
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type : 'text'
    })
    @Field(type => ID)
    @Column({type: 'text', nullable: true })
    public uid?: string;

    @Field(type => Identifiant,{
        description: 'Identifiant de l\'amendement',
        nullable: true
    })
    @OneToOne(type => Identifiant, { nullable : true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public identifiant?: Identifiant;

    @EsField({
        type : 'text'
    })
    @Field({
        nullable: true,
        description: `Gardé pour confort (pour éviter de le reconstituer en XQuery).\n
        C’est le “nom courant” de l’amendement avec le RectN final. Ce n’est PAS un identifiant officiel.\n
        CECI EST EN FAIT UN LIBELLE (comportant éventuellement des espaces) c’est le nom utilisé par les humains pour parler de l’amendement`,
    })
    @Column({type: 'text', nullable: true })
    public numeroLong?: string;

    @Field(type => EtapeTexte,{
        description: 'Etape du texte',
        nullable: true,
    })
    @Column({type: 'text', nullable: true })
    public etapeTexte?: EtapeTexte;

    @EsField({
        type : 'text'
    })
    @Field({
        nullable: true,
        description: 'tri amendement',
    })
    @Column({type: 'text', nullable: true })
    public triAmendement?: string;

    @EsField({
        type : 'text'
    })
    @Field({
        nullable: true,
        description: 'cardinalité multiples d\'amendements',
    })
    @Column({type: 'text', nullable: true })
    public cardinaliteAmdtMultiples?: string;

    @EsField({
        type : 'text'
    })
    @Field({
        nullable: true,
        description: 'Référence amendement parent',
    })    
    @Column({type: 'text', nullable: true })
    public amendementParent?:  string;

    @Field(type => Etat)
    @Column('enum',
    {
        name: 'etat',
        enum: Etat,
        default: Etat.EN_TRAITEMENT,
        nullable: true
    })
    public etat: Etat;

    
    @Field(type => Signataires,{
        description: 'Signataires de l\'amendement',
        nullable: true
    })
    @OneToOne(type => Signataires, { nullable : true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public signataires: Signataires;

    @Field(type => PointeurFragmentTexte)
    @OneToOne(type => PointeurFragmentTexte, { nullable: true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public pointeurFragmentTexte: PointeurFragmentTexte;

    @EsField({
        type: 'object',
        fieldClass: Corps,
        analyzer: 'french_html_corpus'
    })
    @Field(type => Corps,{
        description: 'Corps de l\'amendement',
        nullable: true
    })
    @OneToOne(type => Corps, { nullable: true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public corps: Corps;

    @EsField({
        type: 'object',
        fieldClass: Representations
    })
    @Field(type => Representations, {
        description: 'Définition à trouver',
        nullable: true
    })
    @OneToOne(type => Representations, { nullable: true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public representations?: Representations;

    @EsField({
        type : 'text'
    })
    @Field({
        description: 'référence de la séance de discussion de l\'amendement',
        nullable: true
    })
    @Column({type: 'text', nullable: true })
    public seanceDiscussion?: string;

    @Field(type => Sort,{
        nullable: true
    })
    @OneToOne(type => Sort, { nullable: true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public sort?: Sort;

    @EsField({
        type : "date"
    })
    @Field({
            description: 'Date de dépot',
            nullable: true
    })
    @Column({ type: 'timestamptz', nullable: true })
    public dateDepot: Date;

    @EsField({
        type : "date"
    })
    @Field({
            description: 'Date de distribution',
            nullable: true
    })
    @Column({type: 'timestamptz', nullable: true })
    public dateDistribution: Date;

    @Field({
        description: 'article99',
        nullable: true
    })
    @Column({type: 'text', nullable: true })
    public article99?: string;

    @Field(type => LoiReference,{
        description: 'Loi de référence',
        nullable: true
    })
    @OneToOne(type => LoiReference, {nullable: true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public loiReference: LoiReference;

    @AfterInsert()
    public addToESIndex(): void {
        const logger = new Logger('Amendement Entity');
        const esImporterClient: ESImporterClient = new ESImporterClient();
        try{
            esImporterClient.hasIndex('amendement').then((hasIndex: boolean): void => {
                if(hasIndex){
                    esImporterClient.addToIndex<Amendement>('amendement',this).then((result: ApiResponse) => {
                        logger.info('Amendement '+ this.uid +' added to ES');

                        // Close client connection to avoid connection overflow.
                        // This can happen if the insert is run through a huge batch of amendment import.
                        esImporterClient.close();
                    });
                }
            });
        }catch(e){
            console.error(e);
        }
    }
    
    @AfterInsert()
    public oraclize(): void {
        try{ 
            // new Clotho<Amendement>().deploy<Amendement>(this,this.etat);

            // new Clotho<Amendement>().deploy(this,'');
        }catch(e){
            // this.logger.error(e);
        }
    }

}
