export enum AvantAApresEnum {
    A = 'A',
    APRES = 'Apres',
    APREES = 'Après',
    AVANT = 'Avant',
}
