import { ProgrammeElement } from '.';

import {
    Entity,
    PrimaryGeneratedColumn,
    OneToMany } from 'typeorm';
import { ObjectType, Field } from 'type-graphql';
import { EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Liste des programmes',
})
@Entity({
    name: 'ListeProgrammes',
})
export class ListeProgrammes {

    constructor(listeProgrammes: ListeProgrammes = undefined){
        if(listeProgrammes){
            this.id = listeProgrammes.id;
            this.programme = listeProgrammes.programme.map((programmeElement: ProgrammeElement): ProgrammeElement => {
                return new ProgrammeElement(programmeElement);
            })
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @Field(type => [ProgrammeElement], {
        description: 'Les différents éléments de programme',
        nullable: true
    })
    @OneToMany(type => ProgrammeElement, programmeElement => programmeElement.listeProgrammes, { nullable: true })
    public programme?: ProgrammeElement[];

}
