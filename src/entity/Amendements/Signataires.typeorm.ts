import { Auteur, Cosignataires } from '.';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn
} from 'typeorm';
import { ObjectType, Field } from 'type-graphql';

@ObjectType({
    description: 'Signataires de l\’amendement'
})
@Entity({
    name: 'Signataires',
})
export class Signataires {

    constructor(signataires: Signataires = undefined){
        if(signataires){
            this.id = signataires.id;
            this.auteur = new Auteur(signataires.auteur);
            this.cosignataires = new Cosignataires(signataires.cosignataires);
            this.texteAffichable = signataires.texteAffichable;
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @Field(type => Auteur,{
        description: 'Auteur de l\'amendement',
        nullable: true,
    })
    @OneToOne(type => Auteur, { nullable: true, eager: true, cascade: ['update','remove']})
    @JoinColumn()
    public auteur?: Auteur;

    @Field(type => Cosignataires,{
        description: 'Cosignataires de l\'amendement',
        nullable: true
    })
    @OneToOne(type => Cosignataires, { nullable: true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public cosignataires: Cosignataires;

    @Field({
        description: 'Texte affichable pour l\'amendement',
        nullable: true
    })
    @Column({type: 'text', nullable: true })
    public texteAffichable?: string;

}
