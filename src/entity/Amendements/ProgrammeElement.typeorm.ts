import { ListeProgrammes } from './ListeProgrammes.typeorm';
import { Action } from './Action.enum.typeorm'
import { Ae } from './Ae.typeorm';
import { LignesCredits } from './LignesCredits.typeorm';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    ManyToOne,
    OneToOne,
    JoinColumn
} from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Element de programme',
})
@Entity({
    name: 'ProgrammeElement',
})
export class ProgrammeElement {

    constructor(programmeElement: ProgrammeElement = undefined){
        if(programmeElement){
            this.id = programmeElement.id;
            this.libelle = programmeElement.libelle;
            this.AE = new Ae(programmeElement.AE);
            this.CP = new Ae(programmeElement.CP);
            this.lignesCredits = new LignesCredits(programmeElement.lignesCredits);
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;


    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Libellé',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public libelle: string;

    @EsField({
        type: 'object',
        fieldClass: Ae
    })
    @Field(type => Ae,{
        description: 'AE',
        nullable: true
    })
    @OneToOne(type => Ae, {nullable: true, cascade: ['update','remove']})
    @JoinColumn()
    public  AE?: Ae;

    
    // @EsField({
    //     type: 'object',
    //     fieldClass: Ae
    // })
    @Field(type => Ae,{
        description: 'CP',
        nullable: true
    })
    @OneToOne(type => Ae, {nullable: true, cascade: ['update','remove']})
    @JoinColumn()
    public CP?: Ae;

    @Column('enum',
    {
        name: 'action',
        enum: Action,
        nullable: true
    })
    public action?: Action;


    // @EsField({
    //     type: 'object',
    //     fieldClass: LignesCredits
    // })
    @Field(type => LignesCredits,{
        description: 'Lignes de crédit',
        nullable: true
    })
    @OneToOne(type => LignesCredits, {
        nullable: true,
        cascade: ['update','remove']
    })
    @JoinColumn()
    public lignesCredits?: LignesCredits;

    @ManyToOne(type => ListeProgrammes, listeProgrammes => listeProgrammes.programme,{
        cascade: ['update','remove']
    })
    public listeProgrammes: ListeProgrammes;
}
