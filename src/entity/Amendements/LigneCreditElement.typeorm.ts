import { LignesCredits } from '.';
import { Ae } from './Ae.typeorm';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToOne
} from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Lignes de crédit',
})
@Entity({
    name: 'LigneCreditElement',
})
export class LigneCreditElement {

    constructor(ligneCreditElement: LigneCreditElement = undefined){
        if(ligneCreditElement){
            this.id = ligneCreditElement.id;
            this.libelle = ligneCreditElement.libelle;
            this.AE = new Ae(ligneCreditElement.AE);
            this.CP = new Ae(ligneCreditElement.CP);
        }
    }
    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: "Libellé",
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public libelle: string;

    @EsField({
        type: 'object',
        fieldClass: Ae
    })
    @Field(type => Ae,{
        description: "AE",
        nullable: true
    })
    @OneToOne(type => Ae, { nullable: true, cascade: ['update','remove'] })
    @JoinColumn()
    public AE: Ae;

    @EsField({
        type: 'object',
        fieldClass: Ae
    })
    @Field(type => Ae,{
        description: "CP",
        nullable: true
    })
    @OneToOne(type => Ae, { nullable: true, cascade: ['update','remove'] })
    @JoinColumn()
    public CP: Ae;

    @ManyToOne(type => LignesCredits, ligneCredits => ligneCredits.ligneCredit, { nullable: true, cascade: ['update','remove'] })
    public ligneCredit: LignesCredits;
}
