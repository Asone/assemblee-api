import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity()
@ObjectType({
    description: 'Total des montants'
})
@Entity({
    name: 'Total',
})
export class Total {

    constructor(total: Total = undefined){
        if(total){
            this.id = total.id;
            this.montantNegatif = total.montantNegatif;
            this.montantPositif = total.montantPositif;
            this.solde = total.solde;
        }
    }
    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Montant positif',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public montantPositif?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Montant négatif',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public montantNegatif?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Solde',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public solde?: string;
}
