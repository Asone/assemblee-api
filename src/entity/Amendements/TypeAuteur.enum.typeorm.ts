export enum TypeAuteur {
    DEPUTE = 'Depute',
    GOUVERNEMENT = 'Gouvernement',
    RAPPORTEUR = 'Rapporteur',
}
