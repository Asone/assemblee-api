// import { IsNotEmpty } from 'class-validator';

import {
    Column,
    Entity,
    OneToOne,
    JoinColumn,
    PrimaryGeneratedColumn
} from 'typeorm';
import { DispositifAmdtCredit, AvantAppel } from '.';
import { ObjectType, Field } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';

@EsEntity({
    index: 'corps',
    type: 'Corps'
})
@ObjectType({
    description: 'Corps de l\'amendement',
})
@Entity({
    name: 'Corps',
    orderBy: {
        id: 'ASC',
    },
})
export class Corps {


    constructor(corps: Corps = undefined){
        if(corps){
            this.id = corps.id;
            this.annexeExposeSommaire = corps.annexeExposeSommaire;
            this.dispositif = corps.dispositif;
            this.exposeSommaire = corps.exposeSommaire;
            this.cartoucheDelaiDepotDepasse = corps.cartoucheDelaiDepotDepasse;
            this.avantAppel = corps.avantAppel;
            this.dispositifAmdtCredit = new DispositifAmdtCredit(corps.dispositifAmdtCredit);
        }
    }

    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'keyword',
        analyzer: 'french_html_corpus'
    })
    @Field({
        description: 'Annexe de l\'exposé sommaire',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public annexeExposeSommaire?: string;

    @EsField({
        type: 'keyword',
        analyzer: 'french_html_corpus'
    })
    @Field({
        description: 'Dispositif',
        nullable: true,
    })
    @Column({type: 'text', nullable: true})
    public dispositif?: string;

    @EsField({
        type: 'text',
        analyzer: 'french_html_corpus'
    })
    @Field({
        description: 'Exposé sommaire de l\'amendement',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public exposeSommaire?: string;

    @EsField({
        type: 'text',
        analyzer: 'french_html_corpus'
    })
    @Field({
        description: 'Cartouche de délai de dépot dépassé',
        nullable: true
    })
    @Column({type: 'text', nullable: true})
    public cartoucheDelaiDepotDepasse?: string;

    @Field(type => AvantAppel,{
        description: '???',
        nullable: true
    })
    @OneToOne(type => AvantAppel, {nullable: true, cascade: ['update','remove']})
    @JoinColumn()
    public avantAppel?: AvantAppel;

    @Field(type => DispositifAmdtCredit,{
        description: 'Dispotifif de crédit relatif à l\'amendement',
        nullable: true
    })
    @OneToOne(type => DispositifAmdtCredit, {nullable: true, cascade: ['update','remove']})
    @JoinColumn()
    public dispositifAmdtCredit?: DispositifAmdtCredit;

    public toString(): string {
        return `${this.id}`;
    }

}
