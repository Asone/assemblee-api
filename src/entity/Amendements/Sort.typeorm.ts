import { SortEnSeance } from '.';

import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';
import { ObjectType, Field, registerEnumType } from 'type-graphql';
import { EsEntity, EsField } from 'es-mapping-ts';

registerEnumType(SortEnSeance, {
    name: "SortEnSeance", // this one is mandatory
    description: "Sort de l\'amendement lors de sa délibération en séance dans l\'hémicycle", // this one is optional
})

@EsEntity()
@ObjectType({
    description: 'Sort de l\'amendement',
})
@Entity({
    name: 'Sort',
})
export class Sort {

    constructor(sort: Sort = undefined){
        if(sort){
            this.id = sort.id;
            this.dateSaisie = new Date(sort.dateSaisie);
            this.sortEnSeance = sort.sortEnSeance;
        }
    }
    
    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type : 'date'
    })
    @Field({
        description: 'Date de saisie de l\'amendement',
        nullable: true
    })
    @Column({type: 'timestamptz', nullable: true})
    public dateSaisie?: Date;

    @Field(type => SortEnSeance, {
        description: 'Sort en séance de l\'amendement',
        nullable: true
    })
    @Column({
        type: 'enum',
        enum: SortEnSeance,
        default: SortEnSeance.TOMBE,
    })
    public sortEnSeance?: SortEnSeance;

}
