export enum DivisionType {
    ANNEXE = 'ANNEXE',
    ARTICLE = 'ARTICLE',
    CHAPITRE = 'CHAPITRE',
    TITRE = 'TITRE',
}
