import { CodeMissionPlf } from '.';

import {
    Column,
    Entity,
    PrimaryGeneratedColumn 
} from 'typeorm';
import { registerEnumType, ObjectType, Field } from 'type-graphql';
import { EsField, EsEntity } from 'es-mapping-ts';


registerEnumType(CodeMissionPlf, {
    name: "CodeMissionPlf", // this one is mandatory
    description: "Code de la mission PLF", // this one is optional
})

@EsEntity()
@ObjectType({
    description: 'Mission visée'
})
@Entity({
    name: 'MissionVisee',
})
export class MissionVisee {

    constructor(missionVisee: MissionVisee = undefined){
        if(missionVisee){
            this.id = missionVisee.id;
            this.codeMissionPLF = missionVisee.codeMissionPLF;
            this.libelleMission = missionVisee.libelleMission;
            this.libelleMissionPLF = missionVisee.libelleMissionPLF;
        }
        
    }

    @PrimaryGeneratedColumn()
    public id: string;


    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Identifiant de la mission auprès de l\‘AN',
        nullable: true
    })
    @Column({ type: 'text', nullable: true })
    public idMissionAN?: string;


    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Libellé de la mission',
        nullable: true
    })
    @Column({ type: 'text', nullable: true })
    public libelleMission?: string;

    @EsField({
        type: 'text'
    })
    @Field(type => CodeMissionPlf,{
        description: 'Code de la mission PLF',
        nullable: true
    })
    @Column('enum',
    {
        name: 'code_mission_plf',
        enum: CodeMissionPlf,
        default: CodeMissionPlf.B,
    })
    public codeMissionPLF?: CodeMissionPlf;


    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Libellé de la mission PLF',
        nullable: true
    })
    @Column({ type: 'text', nullable: true })
    public libelleMissionPLF?: string;

}
