import { TypeMimeType } from './TypeMimeType.enum.typeorm';

import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from 'typeorm';

@Entity({
    name: 'TypeMime',
})
export class TypeMime {

    @PrimaryGeneratedColumn()
    public id: string;

    @Column('enum',
    {
        enum: TypeMimeType,
        default: TypeMimeType.APPLICATION,
        nullable: true
    })
    public type: TypeMimeType;

    // @Column('enum',
    // {
    //     enum: Nom,
    //     default: Nom.PDF,
    // })
    // public subType: Nom;

}
