import { Saisine } from '.';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne
} from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import { EsEntity, EsField } from 'es-mapping-ts';


@EsEntity()
@ObjectType({
    description: 'Identifiant object'
})
@Entity({
    name: 'Identifiant',
})
export class Identifiant {

    constructor(identifiant: Identifiant = undefined){
    
        if(identifiant){
            this.id = identifiant.id;
            this.legislature = identifiant.legislature;
            this.numRect = identifiant.numRect;
            this.saisine = new Saisine(identifiant.saisine);
        }

    }

    @PrimaryGeneratedColumn()
    public id: string;

    @EsField({
        type: 'text',
    })
    @Field({
        description: 'Législature',
        nullable: true
    })
    @Column({ type : 'text', nullable: true })
    public legislature?: string;

    @EsField({
        type: 'text'
    })
    @Field({
        description: 'Numéro de rectification',
        nullable: true
    })
    @Column({ type : 'text', nullable: true})
    public numRect?: string;

    @EsField({
        type: 'object',
        fieldClass: Saisine
    })
    @Field(type => Saisine,{
        description: 'Saisine associée à l\'amendement',
        nullable: true
    })
    @OneToOne(type => Saisine, { nullable: true, eager: true, cascade: ['update','remove'] })
    @JoinColumn()
    public saisine: Saisine;

}
