import { Column, BeforeInsert, BeforeUpdate } from 'typeorm';

export abstract class Timestampable{
 
    constructor(timestampable: Timestampable = null){

        if(timestampable){
            this.createdAt = timestampable.createdAt;
            this.updatedAt = timestampable.updatedAt;
        }
    }
    @Column({
        type: 'timestamptz',
        nullable: false,
        default: new Date()
    })
    public createdAt: Date;

    @Column({
        type: 'timestamptz',
        nullable: false,
        default: new Date()
    })
    public updatedAt: Date;

    @BeforeInsert()
    public updateDateCreation(): void {
        this.createdAt = new Date();
    }

    @BeforeUpdate()
    public updateDateUpdate(): void {
        this.updatedAt = new Date();
    }
}
