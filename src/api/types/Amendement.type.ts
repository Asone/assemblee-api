import {
    Field, 
    ID, 
    // Int, 
    ObjectType } from 'type-graphql';

// import { User } from './User';

@ObjectType({
    description: 'Amendement transaction',
})
export class Amendement {

    @Field(type => ID)
    public uuid: string;

    @Field({
        nullable: true,
        description: 'The deployment transaction',
    })
    public tx?: string;

    @Field({
        nullable: true,
        description: 'The amendement address on the Ethereum blockchain',
    })
    public address?: string;

    @Field({
        nullable: true,
        description: 'The deposit date inside the french parlament'
    })
    public deposit_date?: Date;

    @Field({
        nullable: true,
        description: 'The deployment timestamp'
    })
    public deployed_at?: Date;

    @Field({
        nullable: true,
        description: 'The contract state'
    })
    public contract_state?: string;

    @Field({
        nullable: true,
        description: 'The amendement state (from last parlament fetch)'
    })
    public amendement_state?: string;

    @Field({
        nullable: true,
        description: 'The block on which contract was deployed'
    })
    public block?: number;

    // @Field(type => User, {
    //     nullable: true,
    // })
    // public owner: User;

}
