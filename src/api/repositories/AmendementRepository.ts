import { EntityRepository, Repository } from 'typeorm';

import { Amendement } from '../../entity/Amendements/';

@EntityRepository(Amendement)
export class AmendementRepository extends Repository<Amendement> {

    /**
     * Find by user_id is used for our data-loader to get all needed pets in one query.
     */
    public findByUserIds(ids: string[]): Promise<Amendement[]> {
        return this.createQueryBuilder()
            .select()
            // .where(`pet.user_id IN (${ids.map(id => `'${id}'`).join(', ')})`)
            .getMany();
    }

}
