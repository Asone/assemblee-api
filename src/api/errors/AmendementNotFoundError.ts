import { HttpError } from 'routing-controllers';

export class AmendementNotFoundError extends HttpError {
    constructor() {
        super(404, 'Amendement not found!');
    }
}
