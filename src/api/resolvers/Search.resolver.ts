import { 
    Arg, 
    Ctx, 
    Query,
} from 'type-graphql';
import { Service } from 'typedi';

import { Logger, LoggerInterface } from '../../decorators/Logger';
import { Context } from '../Context';
import { AmendementService } from '../services/AmendementService';
import { Amendement } from '../../entity/Amendements/';

@Service()
export class SearchResolver {

    constructor(
        private amendementService: AmendementService,
        @Logger(__filename) private log: LoggerInterface        // @DLoader(UserModel) private userLoader: DataLoader<string, UserModel>
    ) { }

    /**
     * 
     * @param param0 The context of request
     * @param text The text to be found
     * @param limit max results output
     * @param start offset start for output
     * @param since the date to start search
     * @param until the date to stop search
     */
    @Query(returns => [Amendement], {
        description: 'Search for text through elastic search service'
    })
    public search(
        @Ctx() { requestId }: Context, 
        @Arg("text", { nullable: false }) text: string,
        @Arg("limit",{ nullable: true}) limit?: number,
        @Arg("start",{ nullable: true}) start?: number,
        @Arg("since",{ nullable: true}) since?: Date,
        @Arg("until",{ nullable: true}) until?: Date
        ): Promise<Amendement[]> {
            this.log.info(`{${requestId}} Find amendements based on text search`);
            const pagination: any = {};
            const dateRange: any = {};

            if(limit || start){
                if(limit){ pagination.limit = limit };
                if(start){ pagination.start = start }; 
            }

            if(since || until){
                if(since){ dateRange.since = since };
                if(until){ dateRange.until = until };
            }

            return this.amendementService.findFullText(text,pagination,dateRange);
    }

}
