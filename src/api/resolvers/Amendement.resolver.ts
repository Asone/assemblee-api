// import DataLoader from 'dataloader';
import { 
    Arg, 
    Ctx, 
    Query, 
    Resolver,
    Int 
    // Root 
} from 'type-graphql';
import { Service } from 'typedi';

// import { DLoader } from '../../decorators/DLoader';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { Context } from '../Context';
// import { Amendement as AmendementModel } from '../models/Amendement';
// import { User as UserModel } from '../models/User';
import { AmendementService } from '../services/AmendementService';
// import { PetInput } from '../types/input/PetInput';
import { Amendement } from '../../entity/Amendements/';

@Service()
@Resolver(of => Amendement)
export class AmendementResolver {

    constructor(
        private amendementService: AmendementService,
        @Logger(__filename) private log: LoggerInterface,
        // @DLoader(UserModel) private userLoader: DataLoader<string, UserModel>
    ) { }

    @Query(returns => [Amendement])
    public amendements(
        @Ctx() { requestId }: Context, 
        @Arg("limit",{ nullable: true}) limit?: number,
        @Arg("start",{ nullable: true}) start?: number,
        @Arg("since",{nullable: true}) since?: Date,
        @Arg("until",{nullable: true}) until?: Date
        ): Promise<Amendement[]> {
            this.log.info(`{${requestId}} Find all users`);
            const pagination: any = {};
            const dateRange: any = {};

            if(limit || start){
                if(limit){ pagination.limit = limit };
                if(start){ pagination.start = start }; 
            }

            if(since || until){
                if(since){ dateRange.since = since };
                if(until){ dateRange.until = until };
            }
            return this.amendementService.find(pagination,dateRange);

    }

    @Query(returns => Int)
    public countAmendements(
        @Ctx() { requestId }: Context): Promise<number>{
            return this.amendementService.getCount();
        }

    @Query(returns => Amendement)
    public amendement(
        @Ctx() { requestId }: Context,
        @Arg("uuid",{ nullable: false}) uuid : string
    ): Promise<Amendement> {

        this.log.info(`{${requestId}} Find a single user users`);
        return this.amendementService.findByUid(uuid);
    }
    // @Mutation(returns => Pet)
    // public async addPet(@Arg('pet') pet: PetInput): Promise<PetModel> {
    //     const newPet = new PetModel();
    //     newPet.name = pet.name;
    //     newPet.age = pet.age;
    //     return this.petService.create(newPet);
    // }

    // @FieldResolver()
    // public async owner(@Root() pet: AmendementModel): Promise<any> {
    //     if (pet.userId) {
    //         return this.userLoader.load(pet.userId);
    //     }
    //     // return this.userService.findOne(`${pet.userId}`);
    // }

    // user: createDataLoader(UserRepository),

    //     petsByUserIds: createDataLoader(PetRepository, {
    //         method: 'findByUserIds',
    //         key: 'userId',
    //         multiple: true,
    //     }),

}
