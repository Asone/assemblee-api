import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { Logger, LoggerInterface } from '../../decorators/Logger';
import { Amendement } from '../../entity/Amendements/';
import { AmendementRepository } from '../repositories/AmendementRepository';
import { Between, LessThan, LessThanOrEqual, MoreThanOrEqual } from 'typeorm';
import { ApiResponse, Client } from '@elastic/elasticsearch';
import { env } from '../../../src/env';
import { SearchResponse } from 'elasticsearch';
// import { events } from '../subscribers/events';

@Service()
export class AmendementService {

    private client: Client = new Client({
        node: env.elastic.node,
        maxRetries: env.elastic.maxRetries,
        requestTimeout: env.elastic.requestTimeout,
        sniffOnStart: false
      });
      
    constructor(
        @OrmRepository(Amendement) private amendementRepository: AmendementRepository,
        @Logger(__filename) private log: LoggerInterface
    ) { }

    /**
     * Find amendments
     * 
     * @param pagination Pagination options
     * @param dateRange Timerange options
     */
    public find(
        pagination?: {start?: number, limit?: number},
        dateRange?: {since?: Date, until?: Date}
        ): Promise<Amendement[]> {
            const options: any = {
                skip: (pagination && pagination.start ? pagination.start : 0),
                take: (pagination && pagination.limit ? pagination.limit : 50),
                where: {}
            };


            if(dateRange){ // Time range filter

                let dateRangeOpt: any;
                if(dateRange.until && dateRange.since){
                    dateRangeOpt = Between(dateRange.since, dateRange.until);
                }else if(dateRange.until && !dateRange.since){
                    dateRangeOpt = LessThanOrEqual(dateRange.until);
                }else if(dateRange.since && !dateRange.until){
                    dateRangeOpt = MoreThanOrEqual(dateRange.since);
                }

                if(dateRangeOpt){
                    options.where.dateDepot = dateRangeOpt;
                }
            }

            options.relations = ['corps'];

            return this.amendementRepository.find(options);
    }

    /**
     * Count amendements in the database
     */
    public getCount(): Promise<number> {
        return this.amendementRepository.count();
    }

    /**
     * Find an amendment based on its uid
     * 
     * @param uuid The uuid of the amendment
     */
    public findOne(uuid: string): Promise<Amendement | undefined> {
        this.log.info('Find an amendement by its uid');
        return this.amendementRepository.findOne({ where: { uuid }});
    }

    /**
     * Find an amendment based on its uid
     * 
     * @param uuid The uuid of the amendment
     */
    public findByUid(uuid: string): Promise<Amendement | undefined> {
        this.log.info('Find an amendement by its uid');
        return this.amendementRepository.findOne({ where: { uuid }});
    }

    /**
     * 
     * @param text The text to look for
     * @param pagination  Pagination options
     * @param dateRange Time range options
     */
    public findFullText(
        text: string,
        pagination?: {start?: number, limit?: number},
        dateRange?: {since?: Date, until?: Date}
        ) : Promise<Amendement[]> {

            const query: any = {
                index: 'amendement',
                body: {
                        query: {
                            bool: {
                                must: [
                                    {
                                        multi_match: {
                                            query: text,
                                            fields: [
                                                'corps.dispositif',
                                                'corps.exposeSommaire',
                                                'corps.annexeExposeSommaire'
                                            ]
                                        }
                                    }
                                ]
                            }
                        }
                }
            }

            if(pagination){
                if (pagination.start) { query.from = pagination.start };
                if (pagination.limit) { query.size = pagination.limit };
            }
            

            if(dateRange){ // Time range filter

                const datetimeSearchFilter: any = {
                    range:{
                        dateDepot: {}
                    }
                }

                if(dateRange.since){
                    datetimeSearchFilter.range.dateDepot.gte = new Date(dateRange.since);
                }

                if(dateRange.until){
                    datetimeSearchFilter.range.dateDepot.lte = new Date(dateRange.until);
                }

                query.body.query.bool.must.push(datetimeSearchFilter);
            }

            return this.client.search(query).then((result: ApiResponse<SearchResponse<Amendement>>) => {
                return result.body.hits.hits.map((hit): Amendement => {
                    return new Amendement(hit._source);
                });
            }).catch((e) => {
                this.log.error(e);
                throw e;
            })
        }
}
