import {
    Get,
    JsonController,
    OnUndefined,
    Param,
    QueryParam
} from 'routing-controllers';

import { AmendementNotFoundError } from '../errors/AmendementNotFoundError';
import { Amendement } from '../../entity/Amendements';
import { AmendementService } from '../services/AmendementService';


@JsonController('/amendements')
export class AmendementController {

    constructor(
        private amendementService: AmendementService
    ) { }

    @Get()
    public find(
        @QueryParam("limit") limit: number = undefined,
        @QueryParam("start") start: number = undefined,
        @QueryParam("since") since: Date = undefined,
        @QueryParam("until") until: Date = undefined
    ): Promise<Amendement[]> {
        return this.amendementService.find({start, limit}, {since, until});
    }

    /**
     * Count the number of amendments stored in database
     * 
     * @return a Promise with the number of amendments stored
     */
    @Get('/count')
    @OnUndefined(AmendementNotFoundError)
    public count(): Promise<number | undefined> {
        return this.amendementService.getCount();
    }

    /**
     * Fetches an amendment based on its uid
     * 
     * @param uuid The uid of the amendment to fetch
     * 
     * @return The fetched amendment
     */
    @Get('/:uuid')
    @OnUndefined(AmendementNotFoundError)
    public one(@Param('uuid') uuid: string): Promise<Amendement | undefined> {
        return this.amendementService.findOne(uuid);
    }
}
