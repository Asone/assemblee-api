import { JsonController, Get, QueryParam } from 'routing-controllers';
import { DiffImporter } from '../../lib/amendements-importers/DiffImporter';
import { env } from '../../env';
import { Client } from '@elastic/elasticsearch';
import { AmendementService } from '../services/AmendementService';
import { Amendement } from 'src/entity/Amendements';


/**
 * This controller provides with elastic search request bridge. 
 * 
 * It allows to search full-text references within the amendment objects. 
 * 
 * @param limit the number of amendments to return
 * @param start the offset on which to start the output
 * @param since The date from which we can start looking
 * @param until the date from which we must stop looking
 * @param text the text to search for
 * 
 * @return A promise of an array of amendments
 * 
 */
@JsonController('/search')
export class SearchController {

    constructor(
        private amendementService: AmendementService
    ) { }

    @Get()
    public search(
        @QueryParam("limit", {required: false }) limit: number = undefined,
        @QueryParam("start", {required: false }) start: number = undefined,
        @QueryParam("since", { required: false }) since: Date = undefined,
        @QueryParam("until", { required: false}) until: Date = undefined,
        @QueryParam("text", { required: true }) text: string
    ): Promise<Amendement[]> {
        return this.amendementService.findFullText(text, {start, limit},{since, until});
    }
}
