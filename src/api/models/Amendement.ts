// import { IsNotEmpty } from 'class-validator';

import {
    Column,
    Entity,
    // JoinColumn,
    // ManyToOne,
    PrimaryColumn } from 'typeorm';

// import { User } from './User';

@Entity({
    name: 'amendement',
    orderBy: {
        registered_at: 'DESC',
    },
})

export class Amendement {

    @PrimaryColumn('uuid')
    public uuid: string;

    // @IsNotEmpty()
    @Column('text')
    public tx: string;

    // @IsNotEmpty()
    @Column()
    public address: string;

    @Column()
    public deposit_date: Date;

    @Column()
    public registered_at: Date;

    @Column()
    public deployed_at: Date;

    @Column()
    public contract_state: string;

    @Column()
    public amendement_state: string;

    @Column('integer')
    public block: number;

    @Column()
    public blockhash: string;

    @Column()
    public receipt: string;

    public toString(): string {
        return `${this.uuid}`;
    }

}
