import { CronJob } from 'cron';
import { DiffImporter } from '../lib/amendements-importers/DiffImporter';
import { env } from '../env';
import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework-w3tec';
import { Logger } from '../../src/lib/logger';

export const cronLoader: MicroframeworkLoader = (settings: MicroframeworkSettings) => {
    new CronManager()
} 
export class CronManager{

    // private jobs: { [key: string] : CronJob[] } = {};
    private jobs: CronJob[] = [];
    private logger: Logger = new Logger('cron');

    constructor(){
        this.launcher();
    }

    private launcher(): void {
        
        // Launch automatic import every day at 11:30 a.m
        this.jobs.push(new CronJob(
            '* 30 11 * * *',
            () => {
                try{
                    this.logger.debug('Automatic import routine launch.')
                    new DiffImporter(env.app.dirs.diffs).steppedLoader(); 
                }catch(e){
                    this.logger.error('Diff routine failed');
                    
                }
            },
            undefined,
            true)
        );

        this.logger.debug('Cronjobs loaded !');
    }

    public stopJobs(): void {
        this.jobs.forEach((job: CronJob) => {
            job.stop();
        });
    }

    public startJobs(): void {
        this.jobs.forEach((job: CronJob) => {
            job.start();
        })
    }
}
