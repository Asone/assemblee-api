import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework-w3tec';
import { Client, NodeOptions, ApiResponse } from '@elastic/elasticsearch';
import { env } from '../env';
import { EsMappingService, InternalEsMapping } from 'es-mapping-ts';
import { Logger } from '../../src/lib/logger';
import { esAnalyzers } from '../../src/lib/es-analyzers';

export const elasticLoader: MicroframeworkLoader = (settings: MicroframeworkSettings) => {
  new ElasticManager();
}

export class ElasticManager{
    private logger: Logger = new Logger('Elastic Search');
    private esMappingService: EsMappingService = EsMappingService.getInstance();
    constructor(){
        this.load();
    }

    public load(): void {
      try{
        const client = new Client({
            node: env.elastic.node,
            maxRetries: env.elastic.maxRetries,
            requestTimeout: env.elastic.requestTimeout,
            sniffOnStart: false
          });
    

          // const mappings: InternalEsMapping[] = EsMappingService.getInstance().getMappings();

          // mappings.forEach((mapping: InternalEsMapping) => {
          //   if(esAnalyzers[mapping.esmapping.index]){
          //     // config.body = esAnalyzers[mapping.esmapping.index];
          //   }
          //   client.indices.putMapping({
          //     index: mapping.esmapping.index,
          //     body: mapping.esmapping.body}).catch((e:ApiResponse) => {
          //       console.log(mapping.esmapping.body);
          //       console.error(e.body);
          //     });
          // });
          // Object.keys(esAnalyzers).forEach((key: string) => {
          //   this.esMappingService.addSettings(key, esAnalyzers[key].settings);
          // });

          // this.esMappingService.uploadMappings(client).then(() => {

          //   this.logger.info('Elastic Search mapping done');
          // }).catch((e: ApiResponse) => {
          //   console.log('error');
          //   // console.error(EsMappingService.getInstance());
          //   console.error(e.body);
          //   // console.error(e.body.error);
          // });
    
          // mappings.then(() => {
          //   this.logger.info('Elastic Search mapping done');
          //   return;
          // }).then(() => {

          //   const config = {
          //     settings: {
          //       analysis: {
          //         analyzer: {
          //           folding: {
          //             tokenizer: 'standard',
          //             filter: ['lowercase', 'asciifolding']
          //           }
          //         }
          //       }
          //     }
          //   }

          //   return client.indices.putSettings({
          //     index: 'amendement',
          //     body: {
          //       settings: {
          //         analysis: {
          //           analyzer: {
          //             // folding: {
          //             //   tokenizer: 'standard',
          //             //   filter: ['lowercase', 'asciifolding']
          //             // }
          //           }
          //         }
          //       }
          //     }
          //   }).then((response: ApiResponse) => {
          //     console.log(response);
          //   }).catch((error: ApiResponse) => {
          //     console.error(error.body.error);
          //   });

          // }).catch((error: any) => {
          //   this.logger.error('Error thrown while trying to load ES mapping');
          //   this.logger.error(error);
          // })
        }catch(e){
          this.logger.error('An error was thrown while loading Elastic Search. See logs');
          console.error(e);
        }
    
    }
}
