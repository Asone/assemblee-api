import { Subject, Observable } from 'rxjs';
import { getManager, EntityManager } from "typeorm";
import { Logger } from '../logger';
import { ObjectType } from 'typeorm/common/ObjectType';
import { DiffAmendement } from '../../interfaces';
import { Amendement } from '../../entity/Amendements/Amendement.typeorm';
import merge from 'deepmerge';

/**
 * This class intends to help to import a diffed amendment to the database.
 * It provides with different helpers to automate the amendments persistances.
 * 
 * The current class relies strongly on npm deepmerge library. 
 * 
 */
export class AmendementUpdater {

    /**
     * Services
     */
    private manager: EntityManager = getManager();
    private logger: Logger = new Logger('AmdtUpdate');

    /**
     * Local variables
     */
    public localAmendement: Amendement = undefined;
    public updatedAmendement: Amendement = undefined;

    private diff: DiffAmendement;

    constructor(diffAmendement: DiffAmendement, filename: string) {
        this.diff = diffAmendement;
        this.import(filename.replace(/\.diff\.json/, ''));
    }

    /**
     * Processes an amendement from AN Data to persisted as an orm entity.
     * 
     * @param amendement The amendement file to be imported
     * 
     * @return A boolean promise that will be true if promise resolves.
     */
    public import(uid: string): Promise<boolean> {
        this.logger.info('Update amendment with uuid ' + uid);

        // Tries to fetch the amendment requested to be updated.
        return this.findAmendement(uid).then((localAmendement: Amendement) => {
            this.logger.debug('Found amendement with uid ' + uid);
            this.localAmendement = localAmendement;
            // Launch amendement update process
            this.diffHandler(localAmendement);
            return true;
        }).catch((e: any) => {
            this.logger.error('An error happened, couldn\'t process amendment. See stacktrace.');
            this.logger.error(e);
            return false;
        });

    }

    /**
     * Launches the different processes to get the updated object
     * 
     * @param amendement The amendement to be updated
     * 
     * @returns void
     */
    private diffHandler(amendement: Amendement): void {
        this.updatedAmendement = amendement;

        // Apply deletion
        if (Object.keys(this.diff.deleted).length > 0)
        {
            if (Object.keys(this.diff.deleted.amendement).length > 0)
            {
                this.updatedAmendement = merge<Amendement>(this.updatedAmendement, this.diff.deleted.amendement);
            }
        }

        // Apply addition
        if (Object.keys(this.diff.added).length > 0)
        {
            if (Object.keys(this.diff.added.amendement).length > 0)
            {
                this.updatedAmendement = merge<Amendement>(this.updatedAmendement, this.diff.added.amendement);
            }
        }

        // Apply update
        if (Object.keys(this.diff.updated).length > 0)
        {
            if (Object.keys(this.diff.updated.amendement).length > 0)
            {
                this.updatedAmendement = merge<Amendement>(this.updatedAmendement, this.diff.updated.amendement);
            }
        }

        this.persist<Amendement>(new Amendement(this.updatedAmendement)).then((result: any) => {
            this.logger.info('Amendement ' + this.updatedAmendement.uid + ' updated.');
        }).catch((e) => {
            this.logger.error('Error while persisting amendement ' + this.updatedAmendement.uid);
            throw e;
        });
    }

    /**
     * Finds an amendment based on its uid.
     * 
     * @param uid The amendement uid
     * 
     * @returns The amendement as a promise
     */
    private findAmendement(uid: string): Promise<Amendement> {
        return this.manager.findOneOrFail<Amendement>(Amendement, { uid });
    }

    /**
     * Finds an instance of sub-object in the database.
     * @param id The id of the object to find
     * @param type The entity name. It can be a Class type or the entity name as string
     * 
     * @returns A promise of the object. 
     * 
     * @throws Error if no element found.
     */
    private findSubEntity<T>(id: number, entity: (new () => ObjectType<T>) | string): Promise<T> {
        if (typeof entity === 'string')
        {
            return this.manager.findOneOrFail<any>(entity, id);
        } else
        {
            return this.manager.findOneOrFail<T>(new entity(), id);
        }
    }

    /**
     * Persists an entity based on Class type
     * 
     * @param object the object to be persisted
     * 
     * @returns a Promise of the persisted object
     */
    private persist<T>(object: Amendement): Promise<Amendement[]> {
        return this.manager.save<Amendement>([this.removeTimestamps(this.sanitizeNullObjects(object))]);
    }

    /**
     * Recursive method to clean the data from the createdAt.
     * It will also update updatedAt field
     * 
     * @param object The entity or sub entity to process
     * 
     * @returns The modified object
     */
    private removeTimestamps(object: any): any {

        Object.keys(object).forEach((key: string) => {
            if (key === 'createdAt')
            {
                delete object[key];
            } else if (key === 'updatedAt')
            {
                object[key] = new Date();
            } else if (typeof key === 'object' && Object.keys(object[key]).length !== 0)
            {
                object[key] = this.removeTimestamps(object[key]);
            }
        });
        return object;
    }

    /**
     * Recursive method to clean the null objects from the data.
     * 
     * @param object The entity or sub entity to process
     * 
     * @returns The modified object
     */
    private sanitizeNullObjects(object: any): any {
        Object.keys(object).forEach((key: string) => {
            if (typeof object[key] === 'object' && object[key] !== null)
            {
                object[key] = Object.keys(object[key]).length === 0 ? null : this.sanitizeNullObjects(object[key]);
            }
        });

        return object;
    }

}
