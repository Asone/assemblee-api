import { Subject, Observable } from 'rxjs';
import { 
    Auteur,
    Sort, 
    Saisine, 
    Amendement, 
    Identifiant, 
    Signataires, 
    Cosignataires,
    PointeurFragmentTexte,
    Division,
    MissionVisee,
    Corps,
    Representations,
    LoiReference,
    Alinea,
    Contenu,
    Representation,
    StatutRepresentation
    // StatutRepresentation
} from '../../entity/Amendements/index';
import { getManager, EntityManager} from "typeorm";
import { Logger } from '../logger';


/**
 * This class intends to help to import an amendment to the database.
 * It provides with different helpers to automate the amendments persistances.
 * 
 * mapping : The unit processor supports for a mapping of strings
 * in order to populate easily simple fields.
 * 
 * custom populators: For specific fields a custom populator
 * as a typed function can be injected into the processor. 
 * This allows you to process spefic fields on your own.
 * 
 * The coordination is brought through nestable promises
 * in order to process the data in a custom order. 
 * 
 */
export class AmendementImporter{

    /**
     * Services
     */
    private manager: EntityManager = getManager();
    private logger: Logger = new Logger('AmdtImport');
   
    /**
     * Local variables
     */
    private _amendement: Subject<Amendement> = new Subject<Amendement>();
    private _obj: Amendement = undefined;
    public __amendement: Amendement = undefined;

    /**
     * The following variable defines the default mappings to use
     * for amendments entities.
     * 
     * This block's content should be moved to environment file a bit later.
     */
    private mappings: { [key: string] : string[] } =  {
        amendement : [
            'uid',
            'numeroLong',
            'triAmendement',
            'etapeTexte',
            'cardinaliteAmdtMultiples',
            'etat',
            'seanceDiscussion',
            'dateDepot',
            'article99'
        ],
        corps: [
        ],
        sort: [
            'sortEnSeance'
        ],
        identifiant: [
            'legislature',
            'numRect'
        ],
        saisine: [
            'refTexteLegisltatif',
            'numeroPartiePLF',
            'organeExamen',
            'mentionSecondeDeliberation'
        ],
        division: [
            'titre',
            'divisionType',
            'articleDesignationCourte',
            'type',
            'articleAdditionnel',
            'chapitreAdditionnel',
            'urlDivisionTexteVise'
        ],
        signataires: [
            'textAffichable'
        ],
        missionVisee: [
            'idMissionAn',
            'libelleMission',
            'codeMissionPLF',
            'libelleMissionPLF'
        ],
        loiReference: [
            'codeLoi',
            'divisionCodeLoi'
        ],
        representations: [],
        cosignataires: [],
        auteur: [
            'typeAuteur',
            'acteurRef',
            'organeRef',
            'groupePolitiqueRef'
        ],
        pointeurFragmentTexte: [],
        alinea: [
            'numero',
            'alineaDesignation'
        ],
        statutRepresentation: [
            'verbatim',
            'canonique',
            'officielle',
            'transcription',
            'enregistrement'
        ],
        contenu: [
            'documentURI'
        ]
    };

    constructor(amendement: Amendement){
        this.__amendement; 
        this._obj = amendement;
        this.import(this._obj);
    }

    /**
     * Converts nil objects from the dump to null.
     * 
     * @param data the data to be analyzed
     * 
     * @returns boolean
     */
    private isNil(data: any): boolean {
        try{
            if(data === null){ return true };
            if(
                typeof data === 'object' 
                && (data !== null || data !== undefined) 
                && '@xsi:nil' in data
            ){
                return true;
            }
            return false;
        }catch(e){
            console.log(data !== null);
            console.log(data === null);
            this.logger.debug(typeof data);
            this.logger.error(data);
            console.error(e);
            return true;
        }
    }

    /**
     * 
     * Writes in the debug log.
     * 
     * @param type The object type which calls the log
     * @param data The data that can be used to extract data to be injected in the log
     * 
     * @return void
     */
    private populatorDebugLogger<T>(type: string, data: T = undefined): void
    {
        let msg: string = '[' + type + '] Object population success.';
        if(data['uid']){
            msg += '(id: ' + data['uid'] + ')'
        }
        this.logger.debug(msg);
    }

    /**
     * Writes in the error log. 
     * 
     * @param type The object type which calls the error log
     * @param error The error thrown
     * 
     * @return void
     */
    private populatorErrorLogger(type: string, error: any): void 
    {
        this.logger.error('[' +  type + '] Population error.');
        console.error(error);
    }

    /**
     * Generic Populator for persistant entity ready instance.
     * You have to specify the entity class as Type. 
     * 
     * @param key the class key the original data will be attached to
     * @param data data that will be used as source. 
     * @param type a function to instanciate data type object
     * @param mapping An array of strings to auto-populate fields of the persitant object.
     * @Param customPopulator expects a function that takes a source and a target as parameters.
     * It allows you to inject specific value processing for populating an entity.
     *
     * 
     * @returns A promise of the persisted object
     */
    private populate<T>(
        key: string, 
        data: T = undefined, 
        type: (new () => T), 
        mapping: string[] = [], 
        customPopulator: (source: T, target: T) => Promise<T> = undefined
    ): Promise<T> {
       
            return new Promise<T>((resolve: (value?: T | PromiseLike<T>) => void , reject: (reason?: any) => void) => {
                // Null object if no data provided.
                if(!data){ resolve(); }

                // Store locally the source data;
                this[key] = data as T;
                
                // Instantiate new persistent object
                let object = new type();

                // auto-populate the persistant object if mapping provided
                object = this.autoMapper<T>(object,data,mapping);

                // Calls a function as specific fields populators
                if(customPopulator){

                    return customPopulator(object, data).then((entity: T) => {
                        return this.persist<T>(entity).then((result: T) => {
                            resolve(result);
                        }).catch((error: any) => {
                            this.logger.debug('Error on field ' + key);
                            reject(error);
                        });
                    }).catch((error: any) => {
                        this.logger.error('Error on building sub-objects for ' + key);
                        reject(error);
                    });
                } else {
                    return this.persist<T>(object).then((result: T) => {
                        resolve(result);
                    }).catch((error: any) => {
                        this.logger.debug('Error persisting on field ' + key);
                        reject(error);
                    });
                }


        });
    }

    /**
     * Populates fields for an entity persistant object base on a mapped source
     * @param target The target object.
     * @param source The source object.
     * @param fields an array of field keys.
     */
    private autoMapper<T>(target: T, source: T, fields: string[] = undefined): T {
        // Avoid useless iterations
        if(!fields){ return target; }
        
        // populates automatically fields based on mapping
        fields.forEach((field: string) => {

            target[field] = this.isNil(source[field]) ? null : source[field];
        });

        return target;
    };

    /**
     * Persists an entity based on Class type
     * 
     * @param object the object to be persisted
     * 
     * @returns a Promise of the persisted object
     */
    private persist<T>(object: T): Promise<T> {
        return this.manager.save<T>(object);
    }

    /**
     * Processes an amendement from AN Data to be persisted as an orm entity.
     * 
     * @param amendement The amendement to be imported
     * 
     * @return A boolean promise that will be true if promise resolves.
     */
    public import(amendement: Amendement): Promise<boolean>{
        this.logger.debug('Populating amendement...');

        return this.populate<Amendement>('amendement',amendement,Amendement,this.mappings['amendement'],this.amendementPopulator)
        .then((result: Amendement) => {
            this.populatorDebugLogger<Amendement>('amendement',result);
            return result;
        })
        .then((result: Amendement) => {

            // Store the local results
            this._amendement.next(result);
            this.__amendement = result;

            return true;

        }).catch((error) => {

            this.populatorErrorLogger('entité',error);
            throw error;
            
        });
    }

    
    /**
     * Specific populator for `Cosignataires` object
     * 
     * @param target the `Cosignataires` object to be persisted
     * @param source the `Cosignataires` JSON object to use as source
     * 
     * @returns a promise of the `Cosignataires` persisted object.
     */
    private cosignatairesPopulator(target: Cosignataires, source: Cosignataires): Cosignataires {
        target.acteurRef = [];
        source.acteurRef.forEach((acteur: string) => {
            target.acteurRef.push(acteur);
        });
        return target;
    }

    /**
     * Specific populator for `Identifiant` object
     * 
     * @param target the `Identifiant` object to be persisted
     * @param source the `Identifiant` JSON object to use as source
     * 
     * @returns a promise of the `Identifiant` persisted object.
     */
    private identifiantPopulator = (target: Identifiant, source: Identifiant): Promise<Identifiant> => {
        return this.populate<Saisine>('saisine',source.saisine,Saisine,this.mappings['saisine']).then((saisine: Saisine) => {
            target.saisine = saisine;
            return target;
        });
    }

    /**
     * Specific populator for `PointeurFragmentTexte` object
     * 
     * @param target the `PointeurFragmentTexte` object to be persisted
     * @param source the `PointeurFragmentTexte` JSON object to use as source
     * 
     * @returns a promise of the `PointeurFragmentTexte` persisted object.
     */
    private pointeurFragmentTextePopulator = (target: PointeurFragmentTexte, source: PointeurFragmentTexte): Promise<PointeurFragmentTexte> => {
        return Promise.all([
            this.populate<MissionVisee>('missionVisee',source.missionVisee,MissionVisee,this.mappings['missionVisee']),
            this.populate<Division>('division',source.division,Division,this.mappings['division'],this.divisionPopulator),
            this.populate<Alinea>('alinea',source.alinea,Alinea,this.mappings['division'],this.alineaPopulator)
        ]).then((result: [MissionVisee,Division,Alinea]) => {
            target.missionVisee = result[0];
            target.division = result[1];
            target.alinea = result[2];
            return target;
        });
    }

    /**
     * Specific populator for `Division` object
     * 
     * @param target the `Division` object to be persisted
     * @param source the `Division` JSON object to use as source
     * 
     * @returns a promise of the `Division` persisted object.
     */
    private divisionPopulator = (target: Division, source: Division): Promise<Division> => {
        
        // todo : move field to mapping declaration and use isNil method instead
        try{
            source.avant_A_Apres;
            target.avant_A_Apres = typeof source.avant_A_Apres === 'object' ? undefined :  source.avant_A_Apres;
        }catch(e){
            this.logger.warn('Couldn\'t patch a avant_A_Apres field.');
            this.logger.debug(e);
        }

        return new Promise<Division>((resolve,reject) => {
            resolve(target);
        });
    }

    /**
     * Specific populator for `Alinea` object
     * 
     * @param target the `Alinea` object to be persisted
     * @param source the `Alinea` JSON object to use as source
     * 
     * @returns a promise of the `Division` persisted object.
     */
    private alineaPopulator = (target: Alinea, source: Alinea): Promise<Alinea> => {

        // todo : move field to mapping declaration and use isNil method instead
        try{
            source.avant_A_Apres;
            target.avant_A_Apres = typeof source.avant_A_Apres === 'object' ? undefined :  source.avant_A_Apres;
        }catch(e){
            this.logger.warn('Couldn\'t patch a avant_A_Apres field.');
            this.logger.debug(e);
        }


        return new Promise<Alinea>((resolve,reject) => {
            resolve(target);
        });
    }

    /**
     * Specific populator for `Signataires` object
     * 
     * @param target the `Signataires` object to be persisted
     * @param source the `Signataires` JSON object to use as source
     * 
     * @returns a promise of the `Signataires` persisted object.
     */
    private signatairesPopulator = (target: Signataires, source: Signataires): Promise<Signataires> => {
        return Promise.all([
            this.populate<Auteur>('auteur',source.auteur,Auteur,this.mappings['auteur']),
            this.populate<Cosignataires>('cosignataires',source.cosignataires,Cosignataires,this.mappings['cosignataires'])
        ]).then((result: [Auteur,Cosignataires]) => {
            target.auteur = result[0];
            target.cosignataires = result[1];
            return target;
        });
    }

    /**
     * Specific populator for `Signataires` object
     * 
     * @param target the `Signataires` object to be persisted
     * @param source the `Signataires` JSON object to use as source
     * 
     * @returns a promise of the `Signataires` persisted object.
     */
    private representationPopulator = (target: Representation, source: Representation): Promise<Representation> => {
        
        return Promise.all([
            this.populate<Contenu>('contenu',source.contenu,Contenu,this.mappings['contenu']),
            this.populate<StatutRepresentation>('statutRepresentation',source.statutRepresentation,StatutRepresentation,this.mappings['statutRepresentation'])
        ]).then((result: [Contenu,StatutRepresentation]) => {
            target.contenu = result[0];
            target.statutRepresentation = result[1];
            return target;
        });
    }

    /** Specific populator for `Representation` object
     * 
     * @param target the `Amendement` object to be persisted
     * @param source the `Amendement` JSON object to use as source
     * 
     * @returns a promise of the `Amendement` persisted object.
     */
    private representationsPopulator = (target: Representations, source: Representations): Promise<Representations> => {
        return this.populate<Representation>('representation',source.representation,Representation,this.mappings['representation'],this.representationPopulator).then((representation: Representation) => {
            target.representation = representation;
            return target;
        });
        
    }

    
    /** Specific populator for `Sort` object
     * 
     * @param target the `Sort` object to be persisted
     * @param source the `Sort` JSON object to use as source
     * 
     * @returns a promise of the `Amendement` persisted object.
     */
    private sortPopulator = (target: Sort, source: Sort): Promise<Sort> => {

        try{
            target.dateSaisie = typeof source.dateSaisie === 'object' ? undefined :  source.dateSaisie;
        }catch(e){
            this.logger.warn('Couldn\'t patch a dateDistribution field.');
            this.logger.debug(e);
        }

        return new Promise<Sort>((resolve, reject) => {
            resolve(target);
        });
    }

    /**
     * Specific populator for `Amendement` object
     * 
     * @param target the `Amendement` object to be persisted
     * @param source the `Amendement` JSON object to use as source
     * 
     * @returns a promise of the `Amendement` persisted object.
     */
    private amendementPopulator = (target: Amendement, source: Amendement): Promise<Amendement> => {
        
        try{
            target.dateDistribution = typeof source.dateDistribution === 'object' ? undefined :  source.dateDistribution
        }catch(e){
            this.logger.warn('Couldn\'t patch a dateDistribution field.');
            this.logger.debug(e);
        }

        return Promise.all([
            this.populate<Identifiant>('identifiant',source.identifiant,Identifiant,this.mappings['identifiant'],this.identifiantPopulator),
            this.populate<Signataires>('signataires' ,source.signataires,Signataires,this.mappings['signataires'],this.signatairesPopulator),
            this.populate<PointeurFragmentTexte>('pointeurFragmentTexte' ,source.pointeurFragmentTexte,PointeurFragmentTexte,this.mappings['pointeurFragmentTexte'],this.pointeurFragmentTextePopulator),
            this.populate<Representations>('representations' ,source.representations,Representations,this.mappings['representations'],this.representationsPopulator),
            this.populate<Corps>('corps' ,source.corps,Corps,this.mappings['corps'],this.corpsPopulator),
            this.populate<LoiReference>('loiReference',source.loiReference,LoiReference,this.mappings['loiReference']),
            this.populate<Sort>('sort',source.sort,Sort,this.mappings['sort'],this.sortPopulator)
        ]).then(
            (result: [
                Identifiant,
                Signataires,
                PointeurFragmentTexte,
                Representations,
                Corps,
                LoiReference,
                Sort
            ]) => {
                target.identifiant = result[0];
                target.signataires = result[1];
                target.pointeurFragmentTexte = result[2];
                target.representations = result[3];
                target.corps = result[4];
                target.loiReference = result[5];
                target.sort = result[6];
                return target;
            });
    }

    /**
     *  Specific populator for `Corps` object
     * 
     * @param target the `Amendement` object to be persisted
     * @param source the `Amendement` JSON object to use as source
     * 
     * @returns a promise of the `Amendement` persisted object.
     * 
     */
    private corpsPopulator = (target: Corps, source: Corps): Promise<Corps> => {
        target.annexeExposeSommaire = source.annexeExposeSommaire && !this.isNil(source.annexeExposeSommaire) ? this.decodeHtmlCharCodes(source.annexeExposeSommaire) : undefined;
        target.dispositif = source.dispositif && !this.isNil(source.dispositif) ? this.decodeHtmlCharCodes(source.dispositif) : undefined;
        target.exposeSommaire = source.exposeSommaire && !this.isNil(source.exposeSommaire) ? this.decodeHtmlCharCodes(source.exposeSommaire) : undefined;

        return new Promise((resolve) => resolve(target));
    }

    /**
     * Evaluates the charcodes from a string.
     * 
     * @param str The string to be processed
     * 
     * @returns The string with the fixed charcodes
     */
    private decodeHtmlCharCodes(str: string): string { 
        try{
            return str.replace(/(&#(\d+);)/g, (match, capture, charCode) => {
                return String.fromCharCode(charCode);
            });
        }catch(e){
            console.log(str);
            return process.exit();
        }

    }
}
