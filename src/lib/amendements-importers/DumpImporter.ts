
import path from 'path';
import fs from 'fs';
import { AmendementImporter } from './index';
import { Amendement } from '../../entity/Amendements/Amendement.typeorm';
import { env } from '../../env';
import { Logger } from '../../../src/lib/logger';
import { getManager, EntityManager, getRepository } from 'typeorm';

/**
 * This class processes import for Amendments provided by the French parlament.
 * It explores recursively a root directory provided in the constructor
 * and will call an instance of the class AmendementImporter for each amendment json file found. 
 *
 * The Diff importer bases its process on an interval you can customize. 
 * This interval is implemented as import can be an important memory consumption process.
 * Interval helps reducing risks of memory heap.
 * 
 */
export class DumpImporter{

    private manager: EntityManager = getManager();
    private logger: Logger = new Logger('Dump Importer');
  
    private rootDir: string;
    private walksPaths: string[][];
    private jobInterval: NodeJS.Timer;
    private intervalTime: number = env.app.import.interval ? env.app.import.interval : 250;

    constructor(rootDir: string){
        this.rootDir = rootDir;
        this.walksPaths = Array.from(this.walkDir(rootDir));
    }

    /**
     * 
     * @param dir 
     */
    private batch(dir: string): void {
        for (const splittedPath of this.walkDir(dir)) {
            this.iterator(splittedPath);
        }
    }

    /**
     * 
     */
    public steppedLoader(): void {
      this.logger.info('Loading job interval with interval time of ' + this.intervalTime + 'ms.');
      this.jobInterval = setInterval(this.steppedIterator,this.intervalTime);
    }

    private steppedIterator = () => {
      if(this.walksPaths.length === 0){
        clearInterval(this.jobInterval);
      } else { 
        this.iterator(this.walksPaths.shift());
      }
    }

    private isNew(uid: string): Promise<boolean> {
        return this.manager.getRepository<Amendement>(Amendement).findOne({
          where: {uid}
        }).then((result: Amendement) => {
          if(result){
            return false
          }
          return true;
        }).catch((error) => {
          throw error;
        });
    }
    /**
     * 
     * Iterator for a single amendement.
     * 
     * @param dir Path to the file as array of strings
     * 
     * @return The amendement importer instance called
     */
    public iterator(dir: string[]): Promise<AmendementImporter> {
      const filePath: string = dir.join('/');
      const filename: string = dir[(dir.length-1)];

        const file: Amendement = JSON.parse(fs.readFileSync(this.rootDir + '/' + filePath).toLocaleString()).amendement as Amendement;
        
        this.isNew(file.uid).then((isNew: boolean) => { // Performs a verification that the amendment does not exists in db
          if(isNew){ // Is the amendment new (by checking that no uuid corresponds in db)
            return new AmendementImporter(file);
          } else{ 
            this.logger.error('The amendment with uuid ' + (file as Amendement).uid + 'has been found in db')
            return undefined;
          }
        });

      return undefined;
    }

    /**
     * 
     * Generator for files listing.
     * 
     * @param rootDir 
     * @param relativeSplitDir 
     * 
     * @return Iterable of string arrays.
     * 
     */
    private *walkDir(
        rootDir: string,
        relativeSplitDir: string[] = [],
      ): Iterable<string[]> {
        const dir = path.join(rootDir, ...relativeSplitDir)
        for (const filename of fs.readdirSync(dir)) {
          if (filename[0] === ".") {
            continue
          }
          const filePath = path.join(dir, filename)
          const relativeSplitPath = [...relativeSplitDir, filename]
          if (fs.statSync(filePath).isDirectory()) {
            yield* this.walkDir(rootDir, relativeSplitPath)
          } else {
            yield relativeSplitPath
          }
        }
      }      
}
