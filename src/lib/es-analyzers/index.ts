export const esAnalyzers = {
    amendement: {
        settings: {
            analysis: {
                analyzer: {
                  french_html_corpus: {
                    tokenizer : "icu_tokenizer",
                    char_filter:  [ "html_strip" ],
                    filter: [
                        "french_elision",
                        'icu_folding',
                        "lowercase",
                        "french_stemmer",
                        "edge_ngram"
                    ]
                  },
                  french_heavy: {
                    tokenizer: "icu_tokenizer",
                    filter: [
                      "french_elision",
                      "icu_folding",
                      "lowercase",
                      "french_stemmer"
                    ]
                  },
                  french_light: {
                    tokenizer: "icu_tokenizer",
                    filter: [
                      "french_elision",
                      "icu_folding",
                      "lowercase"
                    ]
                  },
                  autocomplete: {
                    tokenizer: "icu_tokenizer",
                    filter: [
                      "french_elision",
                      "icu_folding",
                      "lowercase",
                      "edge_ngram"
                    ]
                  }
                },
                normalizer: {
                  sorting: {
                    filter: [
                      "icu_folding",
                      "lowercase"
                    ]
                  }
                },
                filter: {
                  french_elision: {
                    type: "elision",
                    articles_case: true,
                    articles: [
                      "l",
                      "m",
                      "t",
                      "qu",
                      "n",
                      "s",
                      "j",
                      "d",
                      "c",
                      "jusqu",
                      "quoiqu",
                      "lorsqu",
                      "puisqu"
                    ]
                  },
                  french_stemmer: {
                    type: "stemmer",
                    language: "light_french"
                  },
                  edge_ngram: {
                    type: "edgeNGram",
                    max_gram: 15,
                    min_gram: 2,
                    token_chars: [
                      "letter",
                      "digit"
                    ]
                  }
                },
                tokenizer: {
                  edge_ngram_tokenizer: {
                    max_gram: 15,
                    min_gram: 2,
                    token_chars: [
                      "letter",
                      "digit"
                    ],
                    type: "edgeNGram"
                  }
                }
              }
        }
    },
    corps: {
        settings: {
            analysis: {
                analyzer: {
                  french_html_corpus: {
                    tokenizer : "icu_tokenizer",
                    char_filter:  [ "html_strip" ],
                    filter: [
                        "french_elision",
                        'icu_folding',
                        "lowercase",
                        "french_stemmer",
                        "edge_ngram"
                    ]
                  },
                  french_heavy: {
                    tokenizer: "icu_tokenizer",
                    filter: [
                      "french_elision",
                      "icu_folding",
                      "lowercase",
                      "french_stemmer"
                    ]
                  },
                  french_light: {
                    tokenizer: "icu_tokenizer",
                    filter: [
                      "french_elision",
                      "icu_folding",
                      "lowercase"
                    ]
                  },
                  autocomplete: {
                    tokenizer: "icu_tokenizer",
                    filter: [
                      "french_elision",
                      "icu_folding",
                      "lowercase",
                      "edge_ngram"
                    ]
                  }
                },
                normalizer: {
                  sorting: {
                    filter: [
                      "icu_folding",
                      "lowercase"
                    ]
                  }
                },
                filter: {
                  french_elision: {
                    type: "elision",
                    articles_case: true,
                    articles: [
                      "l",
                      "m",
                      "t",
                      "qu",
                      "n",
                      "s",
                      "j",
                      "d",
                      "c",
                      "jusqu",
                      "quoiqu",
                      "lorsqu",
                      "puisqu"
                    ]
                  },
                  french_stemmer: {
                    type: "stemmer",
                    language: "light_french"
                  },
                  edge_ngram: {
                    type: "edgeNGram",
                    max_gram: 15,
                    min_gram: 2,
                    token_chars: [
                      "letter",
                      "digit"
                    ]
                  }
                },
                tokenizer: {
                  edge_ngram_tokenizer: {
                    max_gram: 15,
                    min_gram: 2,
                    token_chars: [
                      "letter",
                      "digit"
                    ],
                    type: "edgeNGram"
                  }
                }
              }
        }
    }
};

