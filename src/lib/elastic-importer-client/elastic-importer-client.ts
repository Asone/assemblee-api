import { Client, ApiResponse } from "@elastic/elasticsearch";
import { env } from '../../env';
import { Logger } from '../logger';

/**
 * This class intends to provides facilities to import data into the Elastic Search client.
 * It creates a public client connection and provides methods to : 
 *  - add data to an index
 *  - ensure an index exists
 */
export class ESImporterClient{

    public client: Client = new Client({
        node: env.elastic.node,
        maxRetries: env.elastic.maxRetries,
        requestTimeout: env.elastic.requestTimeout,
        sniffOnStart: false
    });

    private logger: Logger = new Logger('ES Importer Client');

    /**
     * This method checks if a specified index exists in the ES Server.
     * 
     * @param index The index to look for
     * 
     * @return Promise<boolean> - Does the index exists ? Returned as a promise.
     */
    public hasIndex(index: string): Promise<boolean> {
        return this.client.indices.exists({ // Checks if the index already exists
            index: [index]
        }).then((result: ApiResponse) => {
            if(result.body === false){ // Index does not exists already
                return false;
            } else { 
                return true
            }
        }).catch((e) => {
            console.log('An error happened retrieving indices !');
            console.error(e);

            return false;
        });
    }

    /**
     * This method adds data to a specified index in ES.
     * 
     * @param index The index to which we want to add data
     * @param body The data to be indexed.
     * 
     * @returns Promise<ApiResponse> - The response from the ES Server
     */
    public addToIndex<T>(index: string, body: T): Promise<ApiResponse> {
        return this.client.index({
            index,
            body
        }).catch((e) => {
            this.logger.error('Adding to index failed. Throwing stacktrace');
            throw e.meta.body;
        });
    }

    /**
     * Closes the client connection to the ES Server. 
     * 
     * This is useful as for batching processes you might want to open multiple connections ( e.g: importing dumps of data).
     * If the connections doesn't get closed manually, at some point you can get a vm.max_map_count error 
     * due to too many connections opened in parallel.
     * 
     * Closing the connection ensures to avoid this situation.
     */
    public close(): void {
        this.client.close();
    }
}
