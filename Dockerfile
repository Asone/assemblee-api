FROM mhart/alpine-node:12 

RUN apk update && apk upgrade && \
    apk add --no-cache git nodejs make g++ python bash openssh-client gcc musl-dev

ADD . /usr/src/app
# Create work directory
WORKDIR /usr/src/app

RUN npm install

# Install runtime dependencies
RUN npm install yarn -g

# Copy app source to work directory

# Install app dependencies
# RUN yarn install
EXPOSE 81
# Build and run the app
CMD npm start serve