# Reinitialize the stack
reset: 
	docker-compose down -v && docker-compose up -d db && docker-compose run api ./node_modules/.bin/ts-node ./node_modules/.bin/typeorm migration:run
prepare-db-schema:
	docker-compose run api ./node_modules/.bin/ts-node ./node_modules/.bin/typeorm migration:run
create-elastic-indices: 
	docker-compose run api ./node_modules/.bin/ts-node ./commands/create-elastic-indices.ts
rebuild-es-index:
	docker-compose run api ./node_modules/.bin/ts-node ./commands/populate-elastic.ts
import-diff:
	docker-compose run api ./node_modules/.bin/ts-node ./commands/amendements-import-diffs.ts
import-dump: 
	docker-compose run api ./node_modules/.bin/ts-node ./commands/amendements-import-dump.ts
migrate: 
	docker-compose run api ./node_modules/.bin/ts-node ./node_modules/.bin/typeorm migrations:run
populate-es:
	docker-compose run api ./node_modules/.bin/ts-node ./commands/populate-elastic.ts
test:
	npm run-script test