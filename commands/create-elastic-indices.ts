import winston from 'winston'
import { env } from '../src/env';
import { format } from 'winston';
import { EsMappingService, EsMapping, InternalEsMapping } from 'es-mapping-ts';
import { Client, NodeOptions, ApiResponse } from '@elastic/elasticsearch';
import { Amendement } from '../src/entity/Amendements';
import { EntityManager, getManager, EntityMetadata, Connection } from 'typeorm';
import { loadConnection, setConnection } from 'typeorm-seeding';
import { Logger } from '../src/lib/logger';
import { RelationMetadata } from 'typeorm/metadata/RelationMetadata';
import { esAnalyzers } from '../src/lib/es-analyzers';


winston.add(new winston.transports.File({ 
    filename: env.app.dirs.logs + '/' + env.log.level + '.log',
    format: env.node !== 'development'
    ? format.combine(
        format.json()
    )
    : format.combine(
        format.colorize(),
        format.simple()
    )
}))
winston.add(new winston.transports.Console({
    level: env.log.level,
    handleExceptions: true,
    format: env.node !== 'development'
        ? format.combine(
            format.json()
        )
        : format.combine(
            format.colorize(),
            format.simple()
        ),
}));


const run = () => {
    new IndicesCreator().createIndices();
}

/**
 * Automates creation of indices based on the mapping provided through es-mapping-ts library declarations.
 */
export class IndicesCreator{
    private client: Client = new Client({
        node: env.elastic.node,
        maxRetries: env.elastic.maxRetries,
        requestTimeout: env.elastic.requestTimeout,
        sniffOnStart: false
      });

    private logger: Logger = new Logger('Elastic Indices creator');

    private mappingService: EsMappingService;


    constructor(){
        // This will load an amendement in order to allow es-mapping-ts to 
        // see the existence of the amendement mapping and all its sub-mappings 
        // We don't need to store the instance. Only to declare it once.
        new Amendement();
    }

    public createIndices(): void {
        const mappings: InternalEsMapping[] = EsMappingService.getInstance().getMappings();
           
        return mappings.forEach((mapping: InternalEsMapping) => {
            if(mapping.esmapping.index) {

                let config: any =  {
                    index: mapping.esmapping.index
                }
                if(esAnalyzers[mapping.esmapping.index]){
                    config.body = esAnalyzers[mapping.esmapping.index];
                }
                
                this.client.indices.create(config).then((response: ApiResponse) => {
                    
                    this.logger.info('Index created for ' + mapping.esmapping.index);
                }).catch((e: ApiResponse) => {
                    this.logger.error('Error for index creation for ' + mapping.esmapping.index);
                    console.error(e.body);
                });
            }
        });
    }
}


run();