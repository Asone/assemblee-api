
import { env } from '../src/env';
import { loadConnection, setConnection } from 'typeorm-seeding';
import winston, { format } from 'winston';
import commander from 'commander';
import { DumpImporter } from '../src/lib/amendements-importers/DumpImporter';

commander
    .version('1.0.0')
    .description('Imports an unzipped dump of the amendments provided by the french parliament')
    .option('--source <path>', 'The dir path where the dump files are located')
    .parse(process.argv);


const sourcePath = (commander.source)
    ? commander.source
    : env.app.dirs.dump;

winston.add(new winston.transports.File({
    filename: env.app.dirs.logs + '/' + env.log.level + '.log',
    format: env.node !== 'development'
        ? format.combine(
            format.json()
        )
        : format.combine(
            format.colorize(),
            format.simple()
        )
}))
winston.add(new winston.transports.Console({
    level: env.log.level,
    handleExceptions: true,
    format: env.node !== 'development'
        ? format.combine(
            format.json()
        )
        : format.combine(
            format.colorize(),
            format.simple()
        ),
}));

const run = async () => {
    try
    {
        const connection = await loadConnection();
        setConnection(connection);
    } catch (error)
    {
        return handleError(error);
    }

    new DumpImporter(sourcePath).steppedLoader();
    return null;
}

const handleError = (error) => {
    console.error(error);
    process.exit(1);
};

run();
