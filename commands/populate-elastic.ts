import winston from 'winston'
import { env } from '../src/env';
import { format } from 'winston';
import { EsMappingService, EsMapping, InternalEsMapping } from 'es-mapping-ts';
import { Client, NodeOptions, ApiResponse } from '@elastic/elasticsearch';
import { Amendement } from '../src/entity/Amendements';
import { EntityManager, getManager, EntityMetadata, Connection } from 'typeorm';
import { loadConnection, setConnection } from 'typeorm-seeding';
import { Logger } from '../src/lib/logger';
import { RelationMetadata } from 'typeorm/metadata/RelationMetadata';
import { esAnalyzers } from '../src/lib/es-analyzers';

const cliProgress = require('cli-progress');

winston.add(new winston.transports.File({ 
    filename: env.app.dirs.logs + '/' + env.log.level + '.log',
    format: env.node !== 'development'
    ? format.combine(
        format.json()
    )
    : format.combine(
        format.colorize(),
        format.simple()
    )
}))
winston.add(new winston.transports.Console({
    level: env.log.level,
    handleExceptions: true,
    format: env.node !== 'development'
        ? format.combine(
            format.json()
        )
        : format.combine(
            format.colorize(),
            format.simple()
        ),
}));

const run = () => {

    // Calls a void instance of the entities we want
    // to be automatically mapped
    // If not called it won't be detected by the mapper.
    new Amendement();
    
    const client = new Client({
        node: env.elastic.node,
        maxRetries: env.elastic.maxRetries,
        requestTimeout: env.elastic.requestTimeout,
        sniffOnStart: false
      });

    try {

        // Loads the connection to the database
        loadConnection().then((connection: Connection): void => {
            setConnection(connection);
            new ElasticIndexer(EsMappingService.getInstance(),client);
        });
    
    } catch (error) {
        return handleError(error);
    }
}

const handleError = (error) => {
    console.error(error);
    process.exit(1);
};

/**
 * This class provides with indexing the data from the postgres ORM database into an ES index. 
 * It is a bridge that will match the mapping name from ES with a similar ORM entity. 
 * 
 * If found, the populator will be called to add the objects in the index of the ES engine.
 * 
 * Note that the indexation can be quite long if there are large amounts of objects in the database.
 * 
 */
export class ElasticIndexer{
    
    private mappingService: EsMappingService;
    private manager: EntityManager = getManager();
    private client: Client;
    private logger: Logger = new Logger('Elastic Populator');
    private progress: {[key: string]: any} = {};
    private multibar: any;
    private baseInterval: number = env.commands.baseInterval ? env.commands.baseInterval : 25;
    private computedInterval: number;
    private entityMetadatas: EntityMetadata[] = [];

    constructor(mappingService: EsMappingService, ESclient: Client) {
        this.mappingService = mappingService;
        this.client = ESclient;

        this.entityMetadatas = this.manager.connection.entityMetadatas;
        
        /**
         * We compute a specific interval based on the number of indexes we will populate.
         * Based on a base interval we define the delay for indexing beetween each object 
         * with the following formula : {base interval}*{number of indexes}
         * 
         * This is to avoid to overflow the limit of the queue of Elastic Search.
         * 
         * This might not be optimal as this value won't be recomputed any time and 
         * it can happen that sometimes an indexation ends, allowing for speeding another one. 
         * 
         */ 
        this.computedInterval = this.baseInterval*this.getIndexes().length;
        
        this.resetIndexes().then((result: boolean) => { // First reset the indexes
            // Launch repopulation indexes.
            this.populateIndexes();
        });
    }

    /**
     * Resets indexes.
     */
    private resetIndexes(): Promise<boolean> {

        return this.client.indices.exists({ // Checks if the index already exists
            index: this.getIndexes()
        }).then((result: ApiResponse) => {
            if(result.body === false){ // Index does not exists already
                return true;
            } else { // Index already exists. We delete it for reset
                return this.deleteIndex();
            }
        }).then((result: boolean) => { // Generate the index
           const mappings: InternalEsMapping[] = EsMappingService.getInstance().getMappings();
           
            return mappings.forEach((mapping: InternalEsMapping) => {
                if(mapping.esmapping.index) {

                    let config: any =  {
                        index: mapping.esmapping.index
                    }
                    if(esAnalyzers[mapping.esmapping.index]){
                        config.body = esAnalyzers[mapping.esmapping.index];
                    }
                    this.client.indices.create(config).then((response: ApiResponse) => {
                        
                        this.logger.info('Index created for ' + mapping.esmapping.index);
                    }).catch((e: ApiResponse) => {
                        this.logger.error('Error for index creation for ' + mapping.esmapping.index);
                        console.error(e.body);
                    });
                }
            });
          
            

        })
        // .then(() => {
        //     return EsMappingService.getInstance().uploadMappings(this.client);
        // })
        .then(() => {
            return true;
        }).catch((error: any) => {
            throw error;
        });
    }

    private getPage(){

    }

    /**
     * Deletes existing indexes.
     */
    private deleteIndex(): Promise<boolean> {
        return this.client.indices.delete({ 
            index: this.getIndexes() // gets the list of indexes to delete them all
        }).then((result: ApiResponse): boolean => {
            this.logger.info('Indexes deleted');
            return true;
        }).catch((error: any) => {
            this.logger.error('An error happened while trying to delete index');
            throw error;
        });
    }

    /**
     * Looks for associated indexes and types for every entity and populates it if needed
     */
    populateIndexes(){

        this.multibar = new cliProgress.MultiBar({
            format: 'Indexing objects for {entity} [{bar}] {percentage}% | {value}/{total}',
            clearOnComplete: false,
            hideCursor: true,
            stopOnComplete: true
        }, cliProgress.Presets.shades_classic);


        for(let entityMetadata of this.entityMetadatas){
            
            if(this.hasORMESMapping(entityMetadata)){ // Process only if the Entity has an associated index

                // Gets all relations from the current entity
                // This is useful to load the entity objects with its relations
                const relations: string[] = entityMetadata.relations.map((relation: RelationMetadata) => {
                    return relation.propertyName;
                })
                try{
                    this.getCountEntity(entityMetadata.name);
                    // Gets the objects of the entity into the database
                    this.manager.getRepository(entityMetadata.name).find(
                        {
                            order: {
                            id: "ASC",
                            },
                            relations: relations
                        }
                    ).then((results: any[]) => {

                        this.progress[entityMetadata.name] = this.multibar.create(results.length, 0,{entity: entityMetadata.name});
                        
                        
                        return results.forEach((result: any, i: number) => {
                            setTimeout(() => { this.index(entityMetadata,result,i)},i*this.computedInterval);
                        });
                    });
                }catch(error){
                    this.logger.error('An error happened while indexing. Aborting...');
                    throw error;
                }
            };
        }

    }

    private getCountEntity(entity: string){
        this.manager.getRepository(entity).count();
    }

    private index(entityMetadata: EntityMetadata,object: any, i: number){
        this.populate(entityMetadata.name.toLocaleLowerCase(),entityMetadata.name,object).then((response: ApiResponse) => {
            this.progress[entityMetadata.name].update((i+1));
        }).catch((e) => {
            this.logger.error('An error happened while indexing object');
            this.progress[entityMetadata.name].update((i+1));
            console.error(e);
        });
    }

    /**
     * 
     * Looks for a match of the entity name in the EsMappings list. 
     * Note that we expect the esMapping.type to have the same name as the entity name.
     *
     * @param entityMetadata the entityMetadata we looking for in the EsMappings
     */
    private hasORMESMapping(entityMetadata: EntityMetadata): boolean {
        const entity: string = this.mappingService.getAllIndex().find((esIndex: string) => {
            return esIndex === entityMetadata.name.toLocaleLowerCase();
        });
        return entity ? true : false;
    }

    private getIndexes(): string[] {
        return this.mappingService.getAllIndex().map((index: string) => {
            return index.toLocaleLowerCase();
        });
    }
    /**
     * 
     * Populates a provided index with an object
     * 
     * @param index The index to populate
     * @param mappingType The mapping type of the object
     * @param object  The object content
     */
    private populate(index: string,mappingType: string, object: any): Promise<ApiResponse> {
        return this.client.index({
            index: index,
            type: mappingType,
            body: object
        }).catch((e) => {
            this.logger.error('Adding to index failed. Throwing ');
            throw e.meta.body;
        });
    }

}

run();