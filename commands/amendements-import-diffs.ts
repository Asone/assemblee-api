import {DiffImporter} from '../src/lib/amendements-importers/DiffImporter';
import {env} from '../src/env';
import { loadConnection, setConnection } from 'typeorm-seeding';
import  winston, { format }  from 'winston';

winston.add(new winston.transports.File({ 
    filename: env.app.dirs.logs + '/' + env.log.level + '.log',
    format: env.node !== 'development'
    ? format.combine(
        format.json()
    )
    : format.combine(
        format.colorize(),
        format.simple()
    )
}))
winston.add(new winston.transports.Console({
    level: env.log.level,
    handleExceptions: true,
    format: env.node !== 'development'
        ? format.combine(
            format.json()
        )
        : format.combine(
            format.colorize(),
            format.simple()
        ),
}));

const run = async () => {
try {
    const connection = await loadConnection();
    setConnection(connection);
  } catch (error) {
    return handleError(error);
  }
  new DiffImporter(env.app.dirs.diffs).steppedLoader();
  return null;
}

const handleError = (error) => {
    console.error(error);
    process.exit(1);
};

  run();